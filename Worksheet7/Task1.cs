﻿using BaseFramework;
using BaseFramework.Dynamics;

namespace Worksheet7
{
    public sealed class Task1 : TaskBase
    {
        public override int SheetNumber => 7;
        public override int TaskNumber => 1;
        
        public override string DescriptiveName => "A binary linked tree.";
        public override bool Unfinished => true;

        public override void ExecuteTask()
        {
            BinaryLinkedTree<int> tree = new BinaryLinkedTree<int>(ComparatorFunc);
            tree.InsertRange(5, 6, 9, 12, 13, 3, 8, 10, 11, 16, 15, 14, 6, 4, 2, 1);

            tree.Visualize();
            tree.Print();
        }

        private int ComparatorFunc(int i, int i1)
        {
            if (i < i1)
                return -1;
            return i == i1 ? 0 : 1;
        }
    }
}
