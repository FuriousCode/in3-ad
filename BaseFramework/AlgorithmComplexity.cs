﻿namespace BaseFramework
{
    /// <summary>   Values that represent algorithm complexities. </summary>
    public enum AlgorithmComplexity
    {
        Linear,
        Logarithmic,
        LinearLogarithmic,
        Squared,
        Cubic,
        Polynomial,
        Exponential
    }
}
