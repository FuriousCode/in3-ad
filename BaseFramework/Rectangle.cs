﻿namespace BaseFramework
{
    /// <summary>   A rectangle. This class cannot be inherited. </summary>
    public struct Rectangle
    {
        ///=================================================================================================
        /// <summary>   Gets or sets the left. </summary>
        ///
        /// <value> The left. </value>
        ///=================================================================================================
        public int Left { get; set; }

        ///=================================================================================================
        /// <summary>   Gets or sets the right. </summary>
        ///
        /// <value> The right. </value>
        ///=================================================================================================
        public int Right { get; set; }

        ///=================================================================================================
        /// <summary>   Gets or sets the top. </summary>
        ///
        /// <value> The top. </value>
        ///=================================================================================================
        public int Top { get; set; }

        ///=================================================================================================
        /// <summary>   Gets or sets the bottom. </summary>
        ///
        /// <value> The bottom. </value>
        ///=================================================================================================
        public int Bottom { get; set; }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="constant"> The constant. </param>
        ///=================================================================================================
        public Rectangle(int constant)
        {
            Left = Top = Right = Bottom = constant;
        }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="left">     The left. </param>
        /// <param name="top">      The top. </param>
        /// <param name="right">    The right. </param>
        /// <param name="bottom">   The bottom. </param>
        ///=================================================================================================
        public Rectangle(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        ///=================================================================================================
        /// <summary>   Returns whether a point is inside the rectangle or not. </summary>
        ///
        /// <param name="x">    The x coordinate. </param>
        /// <param name="y">    The y coordinate. </param>
        ///
        /// <returns>   true if it succeeds, false if it fails. </returns>
        ///=================================================================================================
        public bool Inside(int x, int y)
        {
            bool mirroredX = Left > Right;
            bool mirroredY = Top > Bottom;

            int actualLeft = mirroredX ? Right : Left;
            int actualRight = mirroredX ? Left : Right;
            int actualTop = mirroredY ? Bottom : Top;
            int actualBottom = mirroredY ? Top : Bottom;

            return x >= actualLeft && x <= actualRight && y >= actualTop && y <= actualBottom;
        }
    }
}
