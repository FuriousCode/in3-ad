﻿using System.Net.Configuration;

namespace BaseFramework
{
    /// <summary>   Interface for a task. </summary>
    public abstract class TaskBase
    {
        ///=================================================================================================
        /// <summary>   Gets the sheet number. </summary>
        ///
        /// <value> The sheet number. </value>
        ///=================================================================================================
        public abstract int SheetNumber { get; }

        ///=================================================================================================
        /// <summary>   Gets the task number. </summary>
        ///
        /// <value> The task number. </value>
        ///=================================================================================================
        public abstract int TaskNumber { get; }

        ///=================================================================================================
        /// <summary>   Gets the subtask number. </summary>
        ///
        /// <value> The subtask number. </value>
        ///=================================================================================================
        public virtual int? SubtaskNumber { get; } = null;

        ///=================================================================================================
        /// <summary>   Gets an descriptive name for this task. </summary>
        ///
        /// <value> The descriptive name. </value>
        ///=================================================================================================
        public abstract string DescriptiveName { get; }

        ///=================================================================================================
        /// <summary>   Gets a value indicating whether this instance is non executable (code only). </summary>
        ///
        /// <value> true if non executable, false if not. </value>
        ///=================================================================================================
        public virtual bool NonExecutable { get; } = false;

        public virtual bool Unfinished { get; } = false;

        /// <summary>   Executes the task. </summary>
        public abstract void ExecuteTask();
    }
}
