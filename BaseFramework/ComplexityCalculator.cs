﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework
{
    ///=================================================================================================
    /// <summary>
    ///     A calculator helper class which can calculate certain data for various algorithms and
    ///     their complexities.
    /// </summary>
    ///=================================================================================================
    public static class ComplexityCalculator
    {
        public static int GetMaxFromTime(int baseInput, TimeSpan baseTime, SortingAlgorithmBase algorithm)
        {
            int items = -1;
            int n = 60000/baseTime.Duration().Milliseconds;
            switch (algorithm.Complexity)
            {
                case AlgorithmComplexity.Linear:
                    throw new NotImplementedException();
                    case AlgorithmComplexity.Logarithmic:
                    throw new NotImplementedException();
                    case AlgorithmComplexity.LinearLogarithmic:
                    throw new NotSupportedException();
                case AlgorithmComplexity.Squared:
                    items = baseInput*(int)Math.Sqrt(n);
                    break;
                    case AlgorithmComplexity.Cubic:
                    throw new NotImplementedException();
                    case AlgorithmComplexity.Polynomial:
                    throw new NotImplementedException();
                case AlgorithmComplexity.Exponential:
                    throw new NotImplementedException();
                default:
                    throw new NotSupportedException();
            }

            return items;
        }
    }
}
