﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    public sealed class CountSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        public override string Name => "Count Sort";
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.Linear;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///
        /// <seealso cref="M:BaseFramework.SortingAlgorithmBase.Sort(int[]@,bool)"/>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            Sort(ref data, data.Length, data.Max(d => d));
            StopTracking();
        }

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data"> [in,out] The data. </param>
        /// <param name="n">    The int to process. </param>
        /// <param name="k">    The int to process. </param>
        ///=================================================================================================
        private void Sort(ref int[] data, int n, int k)
        {
            int j = 1;
            int[] temp = new int[k + 1];
            HitAction(4);

            for (int i = 0; i < n; i++)
            {
                temp[data[i]]++;
                HitBoth(2, 1);
            }
            for (int i = 0; i < n; i++)
            {
                HitBoth();

                while (temp[j] == 0)
                {
                    j++;
                    HitBoth();
                }

                data[i] = j;
                temp[j]--;
                HitAction(2);

                if (Visualize)
                    Visualizer.Visualize<int[], int>(_visualizerId, data);
                if (EnableDebugOutput)
                    ArrayOut.Out(data, ConsoleColor.Gray);
                if (SimulateDelay)
                    Task.Delay(Delay).Wait();
            }
        }
    }
}
