﻿using System;
using System.Threading;
using System.Windows.Media;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    // TODO: Implement visualisation
    /// <summary>   An insertion sort algorithm. </summary>
    public sealed class InsertionSorting : SortingAlgorithmBase
    {
        private int _visualizerId;
        /// <summary>   The name. </summary>
        public override string Name => "Insertion Sort";

        /// <summary>   The complexity. </summary>
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.Squared;

        ///=================================================================================================
        /// <summary>   Sorts an integer array. </summary>
        ///
        /// <param name="data">         The data. </param>
        /// <param name="sortInverse">  (Optional) true to sort inverse. </param>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortInverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            if (sortInverse)
                SortInverse(ref data);
            else SortNormal(ref data);
            StopTracking();
        }

        ///=================================================================================================
        /// <summary>   Sorts inverse. </summary>
        ///
        /// <param name="data"> The data. </param>
        ///=================================================================================================
        private void SortInverse(ref int[] data)
        {
            for (int i = 1; i < data.Length; i++)
            {
                int key = data[i];
                int mIndex = i - 1;
                while (mIndex >= 0 && data[mIndex] < key)
                {
                    data[mIndex + 1] = data[mIndex];
                    mIndex--;
                }

                data[mIndex + 1] = key;

                if (EnableDebugOutput)
                {
                    ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                    {
                        new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                        new Tuple<int, ConsoleColor>(mIndex, ConsoleColor.Red),
                        new Tuple<int, ConsoleColor>(mIndex + 1, ConsoleColor.Yellow)
                    });
                }

                if (Visualize)
                {
                    Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                        new Tuple<int, Color>(mIndex, Colors.Red),
                        new Tuple<int, Color>(mIndex + 1, Colors.Yellow));
                }

                if (SimulateDelay)
                    Thread.Sleep(Delay);
            }
        }

        ///=================================================================================================
        /// <summary>   Sorts by maximum. </summary>
        ///
        /// <param name="data"> The data. </param>
        ///=================================================================================================
        private void SortNormal(ref int[] data)
        {
            for (int i = 1; i < data.Length; i++)
            {
                int key = data[i];
                int mIndex = i - 1;
                while (mIndex >= 0 && data[mIndex] > key)
                {
                    data[mIndex + 1] = data[mIndex];
                    mIndex--;
                }

                data[mIndex + 1] = key;
                if (EnableDebugOutput)
                {
                    ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                    {
                        new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                        new Tuple<int, ConsoleColor>(mIndex, ConsoleColor.Red),
                        new Tuple<int, ConsoleColor>(mIndex + 1, ConsoleColor.Yellow)
                    });
                }

                if (Visualize)
                {
                    Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                        new Tuple<int, Color>(mIndex, Colors.Red),
                        new Tuple<int, Color>(mIndex + 1, Colors.Yellow));
                }

                if (SimulateDelay)
                    Thread.Sleep(Delay);
            }
        }
    }
}