﻿using System;
using System.Threading;
using System.Windows.Media;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    /// <summary>   A selection sort algorithm. </summary>
    public sealed class SelectionSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        /// <summary>   The name. </summary>
        public override string Name => "Selection Sort";

        /// <summary>   The complexity. </summary>
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.Squared;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            if (sortReverse)
                SortReverse(ref data);
            else SortNormal(ref data);
            StopTracking();
        }

        private void SortReverse(ref int[] data)
        {
            int previous = -1;
            int inserted = -1;
            for (int i = 0; i < data.Length; i++)
            {
                int min = i;
                for (int x = i; x < data.Length; x++)
                    if (data[x] > data[min]) min = x;

                if (EnableDebugOutput)
                {
                    ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                    {
                        new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                        new Tuple<int, ConsoleColor>(min, ConsoleColor.Yellow),
                        new Tuple<int, ConsoleColor>(previous, ConsoleColor.Green),
                        new Tuple<int, ConsoleColor>(inserted, ConsoleColor.Cyan)
                    });
                }

                if (Visualize)
                {
                    Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                        new Tuple<int, Color>(min, Colors.Yellow),
                        new Tuple<int, Color>(previous, Colors.Green),
                        new Tuple<int, Color>(inserted, Colors.Cyan));
                }

                if (SimulateDelay)
                    Thread.Sleep(Delay);

                int y = data[i];
                data[i] = data[min];
                data[min] = y;
                previous = min;
                inserted = i;
            }
        }

        private void SortNormal(ref int[] data)
        {
            int previous = -1;
            int inserted = -1;
            for (int i = 0; i < data.Length; i++)
            {
                int min = i;
                for (int x = i; x < data.Length; x++)
                    if (data[x] < data[min]) min = x;

                if (EnableDebugOutput)
                {
                    ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                    {
                        new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                        new Tuple<int, ConsoleColor>(min, ConsoleColor.Yellow),
                        new Tuple<int, ConsoleColor>(previous, ConsoleColor.Green),
                        new Tuple<int, ConsoleColor>(inserted, ConsoleColor.Cyan)
                    });
                }

                if (Visualize)
                {
                    Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                        new Tuple<int, Color>(min, Colors.Yellow),
                        new Tuple<int, Color>(previous, Colors.Green),
                        new Tuple<int, Color>(inserted, Colors.Cyan));
                }

                if (SimulateDelay)
                    Thread.Sleep(Delay);

                int y = data[i];
                data[i] = data[min];
                data[min] = y;
                previous = min;
                inserted = i;
            }
        }
    }
}
