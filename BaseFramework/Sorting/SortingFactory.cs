﻿using System;

namespace BaseFramework.Sorting
{
    /// <summary>   A sorting algorithm factory class. </summary>
    public static class SortingFactory
    {
        ///=================================================================================================
        /// <summary>   Makes a new algorithm instance with defined parameters. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="debugging">    true to debugging. </param>
        /// <param name="debugOutput">  (Optional) true to debug output. </param>
        /// <param name="visualize">    (Optional) true to visualize. </param>
        /// <param name="measurement">  (Optional) true to measurement. </param>
        /// <param name="enableDelay">  (Optional) true to enable, false to disable the delay. </param>
        /// <param name="delay">        (Optional) the delay. </param>
        ///
        /// <returns>   A T. </returns>
        ///=================================================================================================
        public static T Make<T>(bool debugging, bool debugOutput = false, bool visualize =false, bool measurement = false, bool enableDelay = false,
            TimeSpan delay = default(TimeSpan)) where T : SortingAlgorithmBase
        {
            SortingAlgorithmBase algo = (SortingAlgorithmBase) Activator.CreateInstance(typeof(T));
            algo.EnableDebugging = debugging;
            algo.EnableDebugOutput = debugOutput;
            algo.EnableRuntimeMeasurement = measurement;
            algo.SimulateDelay = enableDelay;
            algo.Delay = delay.Seconds;
            algo.Visualize = visualize;

            return (T)algo;
        }
    }
}
