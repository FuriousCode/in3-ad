﻿using System.Threading.Tasks;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    public sealed class MapSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        public override string Name => "Map Sort";
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.Linear;

        ///=================================================================================================
        /// <summary>   Gets or sets the field size factor. </summary>
        ///
        /// <value> The field size factor. </value>
        ///=================================================================================================
        public double FieldSizeFactor { get; set; } = 1.25;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///
        /// <seealso cref="M:BaseFramework.SortingAlgorithmBase.Sort(int[]@,bool)"/>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            Sort(ref data, data.Length, FieldSizeFactor);
            StopTracking();
        }

        private void Sort(ref int[] data, int n, double c)
        {
            int newn = (int) (n*c);
            int j = 0;
            int[] temp = new int[newn];
            int min = int.MaxValue;
            int max = int.MinValue;

            if (EnableDebugging)
            {
                ActionCount += 6;
                ComparisionCount += 2*data.Length;
            }

            for (int i = 0; i < newn; i++)
            {
                temp[i] = -1;
                if (!EnableDebugging) continue;
                ActionCount += 2;
                ComparisionCount++;
            }

            for (int i = 0; i < n; i++)
            {
                HitBoth();

                if (data[i] < min)
                {
                    min = data[i];
                    HitBoth();
                }
                else if (data[i] > max)
                {
                    max = data[i];
                    HitBoth();
                }
            }

            double dist = (max - min)/(double)(newn - 1);
            HitAction(5);

            for (int i = 0; i < n; i++)
            {
                HitBoth();

                int t = (int) ((data[i] - min)/dist);
                int insert = data[i];
                int left = 0;
                HitAction(6);
                if (temp[t] != -1 && insert <= temp[t])
                {
                    left = 1;
                    HitAction();
                }
                HitCondition(3);

                while (temp[t] != -1)
                {
                    HitCondition();
                    if (left == 1)
                    {
                        if (insert > temp[t])
                        {
                            int tmp = temp[t];
                            temp[t] = insert;
                            insert = tmp;
                            HitAction(3);
                        }
                        HitCondition();

                        if (t > 0) t--;
                        else left = 0;
                        HitBoth();
                    }
                    else
                    {
                        if (insert <= temp[t])
                        {
                            int tmp = temp[t];
                            temp[t] = insert;
                            insert = tmp;
                            HitAction(3);
                        }
                        HitCondition();

                        if (t < newn - 1) t++;
                        else left = 1;
                        HitBoth(2, 1);
                    }
                }

                temp[t] = insert;
                HitAction();
            }

            for (int i = 0; i < newn; i++)
            {
                HitBoth();
                if (temp[i] != -1)
                {
                    data[j++] = temp[i];
                    HitAction(2);
                }
                HitCondition();

                if (Visualize)
                    Visualizer.Visualize<int[], int>(_visualizerId, data);
                if (EnableDebugOutput)
                    ArrayOut.Out(data);
                if (SimulateDelay)
                    Task.Delay(Delay).Wait();
            }
        }
    }
}
