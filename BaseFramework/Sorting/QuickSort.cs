﻿using System;
using System.Collections.Generic;
using System.Linq;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    /// <summary>   A quick sort algorithm. </summary>
    public sealed class QuickSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        /// <summary>   The name. </summary>
        public override string Name => "Quick Sort";

        /// <summary>   The complexity. </summary>
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.LinearLogarithmic;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///
        /// <seealso cref="M:BaseFramework.SortingAlgorithmBase.Sort(int[]@,bool)"/>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            //if (Visualize)
            //    _visualizerId =
            //        Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
            //            DefaultGenerators.IntArray.GenerateEdges).Result;

            data = Quicksort(data.ToList()).ToArray();
        }

        public static List<int> Quicksort(List<int> elements)
        {
            Random rnd = new Random();
            if (elements.Count() < 2) return elements;
            int pivot = rnd.Next(elements.Count());
            int val = elements[pivot];
            List<int> lesser = new List<int>();
            List<int> greater = new List<int>();
            for (int i = 0; i < elements.Count(); i++)
            {
                if (i != pivot)
                {
                    if (elements[i].CompareTo(val) < 0)
                    {
                        lesser.Add(elements[i]);
                    }
                    else
                    {
                        greater.Add(elements[i]);
                    }
                }
            }

            List<int> merged = Quicksort(lesser);
            merged.Add(val);
            merged.AddRange(Quicksort(greater));
            return merged;
        }
    }
}