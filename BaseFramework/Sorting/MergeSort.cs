﻿using System;
using System.Threading;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    ///=================================================================================================
    /// <summary>   A merge sort algorithm. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:BaseFramework.SortingAlgorithmBase"/>
    ///=================================================================================================
    public sealed class MergeSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        public override string Name => "Merge Sort";
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.LinearLogarithmic;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///
        /// <seealso cref="M:BaseFramework.SortingAlgorithmBase.Sort(int[]@,bool)"/>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            Sort(ref data, 0, data.Length - 1);
            StopTracking();
        }

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">     [in,out] The data. </param>
        /// <param name="first">    The first. </param>
        /// <param name="last">     The last. </param>
        ///=================================================================================================
        private void Sort(ref int[] data, int first, int last)
        {
            if (first >= last) return;
            int m = (first + last)/2;
            Sort(ref data, first, m);
            Sort(ref data, m + 1, last);
            Merge(ref data, first, last, m + 1);

            if (EnableDebugOutput)
                ArrayOut.Out(data);
            if (Visualize)
                Visualizer.Visualize<int[], int>(_visualizerId, data);
            if (SimulateDelay)
                Thread.Sleep(Delay);
        }

        ///=================================================================================================
        /// <summary>   Merges the arrays. </summary>
        ///
        /// <param name="data">     [in,out] The data. </param>
        /// <param name="first">    The first. </param>
        /// <param name="last">     The last. </param>
        /// <param name="middle">   The middle. </param>
        ///=================================================================================================
        private void Merge(ref int[] data, int first, int last, int middle)
        {
            int n = last - first + 1;
            int a1F = first;
            int a1L = middle - 1;
            int a2F = middle;
            int a2L = last;

            int[] merged = new int[n];

            for (int i = 0; i < n; i++)
            {
                if (a1F <= a1L)
                {
                    if (a2F <= a2L)
                    {
                        if (data[a1F] <= data[a2F])
                            merged[i] = data[a1F++];
                        else merged[i] = data[a2F++];
                    }
                    else merged[i] = data[a1F++];
                }
                else merged[i] = data[a2F++];
            }

            for (int i = 0; i < n; i++)
                data[first + i] = merged[i];
        }
    }
}
