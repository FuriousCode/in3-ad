﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Documents;
using System.Windows.Media;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    public sealed class HeapSort : SortingAlgorithmBase
    {
        private int _visualizerId;
        public override string Name => "Heap Sort";
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.LinearLogarithmic;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///
        /// <seealso cref="M:BaseFramework.SortingAlgorithmBase.Sort(int[]@,bool)"/>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortReverse = false)
        {
            int first = 0;
            int last = data.Length - 1;

            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            BuildHeap(ref data, first, last);
            
            for (int i = last; i > first; i--)
            {
                int tmp = data[first];
                data[first] = data[i];
                data[i] = tmp;

                if (EnableDebugging)
                {
                    ComparisionCount++;
                    ActionCount += 4;
                }

                MakeHeap(ref data, first, i - 1, first);
            }
            StopTracking();
        }

        private int _debugPrevLargest = -1;
        private void MakeHeap(ref int[] data, int first, int last, int root)
        {
            int left = first + (root - first)*2 + 1;
            int right = first + (root - first)*2 + 2;
            int largest;

            if (EnableDebugging)
                ActionCount += 10;

            if (left <= last && data[left] > data[root])
                largest = left;
            else largest = root;

            if (right <= last && data[right] > data[largest])
                largest = right;

            if (EnableDebugging)
            {
                ComparisionCount += 6;
                ActionCount += 2;
            }

            if (largest == root)
            {
                if (EnableDebugging)
                    ComparisionCount++;
                return;
            }

            if (EnableDebugOutput)
                ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray,
                    new Tuple<int, ConsoleColor>(largest, ConsoleColor.Green),
                    new Tuple<int, ConsoleColor>(root, ConsoleColor.Red),
                    new Tuple<int, ConsoleColor>(_debugPrevLargest, ConsoleColor.Cyan));
            if (Visualize)
            {
                List<Tuple<int, Color>> tuples = new List<Tuple<int, Color>>(3);
                if (largest >= 0)
                    tuples.Add(new Tuple<int, Color>(data[largest], Colors.Green));
                if (root >= 0)
                    tuples.Add(new Tuple<int, Color>(data[root], Colors.Red));
                if (_debugPrevLargest >= 0)
                    tuples.Add(new Tuple<int, Color>(data[_debugPrevLargest], Colors.Cyan));

                Visualizer.Visualize(_visualizerId, data, tuples.ToArray());
            }

            if (SimulateDelay)
                Thread.Sleep(Delay);

            int tmp = data[root];
            data[root] = data[largest];
            data[largest] = tmp;

            _debugPrevLargest = root;

            if (EnableDebugging)
                ActionCount += 3;

            MakeHeap(ref data, first, last, largest);
        }

        private void BuildHeap(ref int[] data, int first, int last)
        {
            int n = last - first + 1;
            if (EnableDebugging)
                ActionCount += 3;

            for (int i = first + (n - 2)/2; i >= first; i--)
            {
                if (EnableDebugging)
                {
                    ActionCount += 4;
                    ComparisionCount++;
                }

                MakeHeap(ref data, first, last, i);
            }
        }
    }
}
