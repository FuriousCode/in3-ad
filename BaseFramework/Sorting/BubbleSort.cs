﻿using System;
using System.Threading;
using System.Windows.Media;
using BaseFramework.Visualisation;

namespace BaseFramework.Sorting
{
    // TODO: Implement visualisation
    /// <summary>   A bubble sort algorithm. </summary>
    public class BubbleSort : SortingAlgorithmBase
    {
        private int _visualizerId;

        /// <summary>   The name. </summary>
        public override string Name => "Bubble Sort";

        /// <summary>   The complexity. </summary>
        public override AlgorithmComplexity Complexity => AlgorithmComplexity.Squared;

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         [in,out] The data. </param>
        /// <param name="sortInverse">  (Optional) true to sort inverse. </param>
        ///=================================================================================================
        public override void Sort(ref int[] data, bool sortInverse = false)
        {
            if (Visualize)
                _visualizerId =
                    Visualizer.ShowWindow<int[], int>(Name, DefaultGenerators.IntArray.GenerateVertices,
                        DefaultGenerators.IntArray.GenerateEdges).Result;

            ResetDebugData();
            StartTracking();
            if (sortInverse)
                SortInverse(ref data);
            else SortNormal(ref data);
            StopTracking();
        }

        private void SortInverse(ref int[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                for (int a = data.Length - 2; a >= i; a--)
                {
                    if (data[a] >= data[a + 1]) continue;

                    if (EnableDebugOutput)
                    {
                        ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                        {
                            new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                            new Tuple<int, ConsoleColor>(a, ConsoleColor.Yellow),
                            new Tuple<int, ConsoleColor>(a + 1, ConsoleColor.Red)
                        });
                    }

                    if (Visualize)
                    {
                        Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                            new Tuple<int, Color>(a, Colors.Yellow),
                            new Tuple<int, Color>(a + 1, Colors.Red));
                    }

                    if (SimulateDelay)
                        Thread.Sleep(Delay);

                    int x = data[a];
                    data[a] = data[a + 1];
                    data[a + 1] = x;
                }
            }
        }

        private void SortNormal(ref int[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                for (int a = data.Length - 2; a >= i; a--)
                {
                    if (data[a] <= data[a + 1]) continue;

                    if (EnableDebugOutput)
                    {
                        ArrayOut.OutHighlighted(data, ConsoleColor.DarkGray, new[]
                        {
                            new Tuple<int, ConsoleColor>(i, ConsoleColor.Magenta),
                            new Tuple<int, ConsoleColor>(a, ConsoleColor.Yellow),
                            new Tuple<int, ConsoleColor>(a + 1, ConsoleColor.Red)
                        });
                    }

                    if (Visualize)
                    {
                        Visualizer.Visualize(_visualizerId, data, new Tuple<int, Color>(i, Colors.Magenta),
                            new Tuple<int, Color>(a, Colors.Yellow),
                            new Tuple<int, Color>(a + 1, Colors.Red));
                    }

                    if (SimulateDelay)
                        Thread.Sleep(Delay);

                    int x = data[a];
                    data[a] = data[a + 1];
                    data[a + 1] = x;
                }
            }
        }
    }
}
