﻿using System;

namespace BaseFramework.Hashing
{
    public sealed class DoubleHashTable : HashTableBase
    {
        private readonly Func<int, int, int> _h1;
        private readonly Func<int, int, int> _h2;

        public DoubleHashTable(int initialCount, Func<int, int, int> h1, Func<int, int, int> h2) : base(initialCount)
        {
            _h1 = h1;
            _h2 = h2;
        }

        protected override Func<int, int, int> HashMethod => Hash;

        private int Hash(int i, int i1)
        {
            if (i1 == 0)
                return i % Size;
            return (_h1(i, Size) + i1*_h2(i, Size)) % Size;
        }
    }
}
