﻿using System;

namespace BaseFramework.Hashing
{
    public sealed class LinearHashTable : HashTableBase
    {
        protected override Func<int, int, int> HashMethod => HashingMethod;

        public LinearHashTable(int initialCount) : base(initialCount)
        {
        }

        private int HashingMethod(int i, int i1)
        {
            return (i + i1) % Size;
        }
    }
}
