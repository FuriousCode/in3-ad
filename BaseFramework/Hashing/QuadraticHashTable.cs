﻿using System;

namespace BaseFramework.Hashing
{
    public sealed class QuadraticHashTable : HashTableBase
    {
        private readonly int _c1;
        private readonly int _c2;

        public QuadraticHashTable(int initialCount, int c1, int c2) : base(initialCount)
        {
            _c1 = c1;
            _c2 = c2;
        }

        protected override Func<int, int, int> HashMethod => Hash;

        private int Hash(int i, int i1)
        {
            if (i1 == 0)
                return i % Size;
            return (i % Size + _c1 * i1 + _c2 * (int)Math.Pow(i1, 2)) % Size;
        }
    }
}
