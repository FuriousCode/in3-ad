﻿using System;

namespace BaseFramework.Hashing
{
    /// <summary>
    /// A base class for a hash table.
    /// </summary>
    public abstract class HashTableBase
    {
        public int Count { get; private set; }

        public bool AllowResizing { get; set; } = false;

        public int Size => _hashTable.Length;

        protected abstract Func<int, int, int> HashMethod { get; }

        private int?[] _hashTable;

        protected HashTableBase(int initialCount)
        {
            _hashTable = new int?[initialCount];
        }

        public void Add(int value)
        {
            int hash = Hash(value);
            _hashTable[hash] = value;
            Count++;

            if (AllowResizing && (double)Count / _hashTable.Length >= 0.75)
                Resize();
        }

        public void AddRange(params int[] values)
        {
            foreach (int value in values)
                Add(value);
        }

        public void Print()
        {
            for (int i = 0; i < _hashTable.Length; i++)
            {
                ConsoleEx.Write($"[{i}]: ");
                if (_hashTable[i] == null)
                    ConsoleEx.Write("null\n", ConsoleColor.DarkGray);
                else ConsoleEx.Write(_hashTable[i].Value + "\n", ConsoleColor.Green);
            }
        }

        private int Hash(int value)
        {
            int offset = 0;
            int hash;
            while (true)
            {
                hash = HashMethod(value, offset);
                if (_hashTable[hash] == null)
                    break;
                offset++;
            }

            return hash;
        }

        private void Resize()
        {
            Array.Resize(ref _hashTable, _hashTable.Length + (int) (_hashTable.Length * 0.333));
        }
    }
}
