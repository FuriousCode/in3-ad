﻿namespace BaseFramework
{
    /// <summary>   Extension methods for arrays. </summary>
    public static class ArrayExtensions
    {
        ///=================================================================================================
        /// <summary>   A T[,] extension method that converts an array to a double array (T[][]). </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="array">    The array to act on. </param>
        ///
        /// <returns>   array as a T[][]. </returns>
        ///=================================================================================================
        public static T[][] ToDoubleArray<T>(this T[,] array)
        {
            int width = array.GetLength(0);
            int height = array.GetLength(1);

            T[][] conv = new T[height][];
            for (int y = 0; y < height; y++)
            {
                conv[y] = new T[width];
                for (int x = 0; x < width; x++)
                {
                    conv[y][x] = array[x, y];
                }
            }

            return conv;
        }
    }
}
