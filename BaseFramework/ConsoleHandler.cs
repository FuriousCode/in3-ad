﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Win32.SafeHandles;
using Application = System.Windows.Application;

namespace BaseFramework
{
    public static class ConsoleHandler
    {
        [DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetStdHandle(int nStdHandle, IntPtr hHandle);

        [DllImport("kernel32.dll")]
        private static extern bool DuplicateHandle(IntPtr hSourceProcessHandle, SafeFileHandle hSourceHandle,
            IntPtr hTargetProcessHandle, out SafeFileHandle lpTargetHandle, uint dwDesiredAccess, bool bInheritHandle,
            uint dwOptions);

        private const uint SW_HIDE = 0;
        private const uint SW_SHOWNORMAL = 1;
        private const uint SW_MAXIMIZE = 3;
        private const uint SW_SHOWNOACTIVATE = 4;
        private const uint SW_RESTORE = 9;
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, uint nCmdShow);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        private const int STD_OUTPUT_HANDLE = -11;
        private const int STD_INPUT_HANDLE = -10;
        private const int STD_ERROR_HANDLE = -12;

        // Disable Console Exit Button
        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll")]
        private static extern IntPtr DeleteMenu(IntPtr hMenu, uint uPosition, uint uFlags);

        private const uint SC_CLOSE = 0xF060;
        private const uint MF_BYCOMMAND = (uint)0x00000000L;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CreateFile([MarshalAs(UnmanagedType.LPTStr)] string filename,
                                       [MarshalAs(UnmanagedType.U4)]     uint access,
                                       [MarshalAs(UnmanagedType.U4)]     FileShare share,
                                                                         IntPtr securityAttributes,
                                       [MarshalAs(UnmanagedType.U4)]     FileMode creationDisposition,
                                       [MarshalAs(UnmanagedType.U4)]     FileAttributes flagsAndAttributes,
                                                                         IntPtr templateFile);

        private const uint GENERIC_WRITE = 0x40000000;
        private const uint GENERIC_READ = 0x80000000;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy,
            uint flags);

        private const uint SWP_ASYNCWINDOWPOS = 0x4000;
        private const uint SWP_NOSIZE = 0x0001;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLongPtr32(HandleRef hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(HandleRef hWnd, int nIndex, IntPtr dwNewLong);

        private static IntPtr SetWindowLongPtr(HandleRef hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            return SetWindowLongPtr(hWnd, nIndex, dwNewLong);
        }

        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        private static extern IntPtr GetWindowLongPtr32(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr")]
        private static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);

        private static IntPtr GetWindowLongPtr(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size == 8)
                return GetWindowLongPtr64(hWnd, nIndex);
            return GetWindowLongPtr32(hWnd, nIndex);
        }

        [DllImport("user32.dll")]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, RedrawWindowFlags flags);

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_LAYERED = 0x00080000;

        /// <summary> Disables exiting via console. </summary>
        public static void DisableConsoleExit()
        {
            IntPtr handle = GetConsoleWindow();
            IntPtr exitButton = GetSystemMenu(handle, false);
            DeleteMenu(exitButton, SC_CLOSE, MF_BYCOMMAND);
        }

        ///=================================================================================================
        /// <summary>   Shows the console window.   </summary>
        ///
        /// <remarks>   Zap, 17.09.2015.    </remarks>
        ///=================================================================================================
        public static void Show(bool activate = false)
        {
            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero)
            {
                if (!Enable())
                    throw new Exception("Console allocation or redirection failed.");
            }
            ShowWindow(handle, activate ? SW_SHOWNORMAL : SW_SHOWNOACTIVATE);
        }

        ///=================================================================================================
        /// <summary>   Hides the console window.   </summary>
        ///
        /// <remarks>   Zap, 17.09.2015.    </remarks>
        ///=================================================================================================
        public static void Hide()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
        }

        /// <summary> Makes the console fullscreen. </summary>
        public static void MakeFullscreen()
        {
            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero) return;

            ShowWindow(handle, SW_MAXIMIZE);
        }

        /// <summary> Makes the console windowed. </summary>
        public static void MakeWindowed()
        {
            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero) return;

            ShowWindow(handle, SW_RESTORE);
        }
        /// <summary> Moves the console to the primary monitor. </summary>
        public static void MoveToPrimaryMonitor()
        {
            Screen[] screens = Screen.AllScreens;
            for (int i = 0; i < screens.Length; i++)
                if (screens[i].Primary)
                    MoveToMonitor(i);
        }

        /// <summary> Moves the console to the secondary monitor (or primary if none exists). </summary>
        public static void MoveToSecondaryMonitor()
        {
            MoveToMonitor(1);
        }

        /// <summary> Moves the console to the tertiary monitor (or secondary if none exists). </summary>
        public static void MoveToTertiaryMonitor()
        {
            MoveToMonitor(2);
        }

        ///=================================================================================================
        /// <summary> Moves the console to the given monitor. </summary>
        ///
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="index">            Zero-based index of the monitor. </param>
        /// <param name="throwIfNotExists"> (Optional) True to throw if not exists. </param>
        ///=================================================================================================
        public static void MoveToMonitor(int index, bool throwIfNotExists = false)
        {
            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero) return;

            Screen[] screens = Screen.AllScreens;
            int screenIndex = -1;
            if (index >= screens.Length && throwIfNotExists)
                throw new Exception(
                    $"Invalid monitor index. Highest monitor index: {screens.Length - 1} vs. index {index}.");
            if (index >= screens.Length)
                screenIndex = screens.Length - 1;

            Screen screen = screens[screenIndex >= 0 ? screenIndex : index];

            SetWindowPos(handle, new IntPtr(-2), screen.Bounds.X, screen.Bounds.Y, 10,
                10, SWP_ASYNCWINDOWPOS | SWP_NOSIZE);

            int error = Marshal.GetLastWin32Error();
            if (error != 0)
                throw new Win32Exception(error);
        }

        ///=================================================================================================
        /// <summary>
        ///     Enables the console for usage. Show will call this automatically if it hasn't been called
        ///     yet.
        /// </summary>
        ///
        /// <returns> True if it succeeds, false if it fails. </returns>
        ///=================================================================================================
        public static bool Enable()
        {
            if (!AttachConsole(-1))
            {
                bool result = AllocConsole();
                if (result)
                    OverrideRedirection();
                return result;
            }
            return true;
        }

        ///=================================================================================================
        /// <summary> Sets opacity in percent. </summary>
        ///
        /// <param name="percentage"> The percentage. </param>
        ///=================================================================================================
        public static void SetOpacityPercentage(double percentage)
        {
            // Normalize if non-normalized value
            if (percentage > 1.0)
                percentage = percentage / 100;
            if (percentage > 1.0)
                percentage = 1.0;
            // Check for negative values
            if (percentage < 0)
                percentage = 0;
            byte alpha = (byte)(percentage * 255);
            SetOpacity(alpha);
        }

        ///=================================================================================================
        /// <summary> Sets the console window opacity. </summary>
        ///
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ///
        /// <param name="alpha"> The alpha. </param>
        ///=================================================================================================
        public static void SetOpacity(byte alpha)
        {
            if (alpha == byte.MaxValue)
            {
                MakeOpaque();
                return;
            }

            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero) return;

            int error;

            long winFlags = GetWindowLongPtr(handle, GWL_EXSTYLE).ToInt64();
            winFlags |= WS_EX_LAYERED;
            if (SetWindowLongPtr(new HandleRef(Application.Current, handle), GWL_EXSTYLE, new IntPtr(winFlags)).ToInt64() == 0)
            {
                error = Marshal.GetLastWin32Error();
                throw new Win32Exception(error);
            }

            if (!SetLayeredWindowAttributes(handle, 0, alpha, 0x2))
            {
                error = Marshal.GetLastWin32Error();

                if (error != 0)
                    throw new Win32Exception(error);
            }
        }

        ///=================================================================================================
        /// <summary> Makes the console window opaque. </summary>
        ///
        /// <exception cref="Win32Exception"> Thrown when a Window 32 error condition occurs. </exception>
        ///=================================================================================================
        public static void MakeOpaque()
        {
            IntPtr handle = GetConsoleWindow();
            if (handle == IntPtr.Zero) return;

            int error;

            // Remove WS_EX_LAYERED
            long winFlags = GetWindowLongPtr(handle, GWL_EXSTYLE).ToInt64();
            winFlags &= ~WS_EX_LAYERED;
            if (SetWindowLongPtr(new HandleRef(Application.Current, handle), GWL_EXSTYLE, new IntPtr(winFlags)).ToInt64() == 0)
            {
                error = Marshal.GetLastWin32Error();
                throw new Win32Exception(error);
            }

            // Redraw console
            if (!RedrawWindow(handle, IntPtr.Zero, IntPtr.Zero,
                RedrawWindowFlags.Erase | RedrawWindowFlags.Invalidate | RedrawWindowFlags.Frame |
                RedrawWindowFlags.AllChildren))
            {
                error = Marshal.GetLastWin32Error();
                if (error != 0)
                    throw new Win32Exception(error);
            }
        }

        /// <summary> Overrides redirection. </summary>
        private static void OverrideRedirection()
        {
            var hOut = GetStdHandle(STD_OUTPUT_HANDLE);
            var hRealOut = CreateFile("CONOUT$", GENERIC_READ | GENERIC_WRITE, FileShare.Write, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (hRealOut != hOut)
            {
                SetStdHandle(STD_OUTPUT_HANDLE, hRealOut);
                Console.SetOut(new StreamWriter(Console.OpenStandardOutput(), Console.OutputEncoding) { AutoFlush = true });
            }
        }
    }
}
