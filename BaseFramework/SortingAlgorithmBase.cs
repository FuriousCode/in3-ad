﻿using System;
using System.Diagnostics;

namespace BaseFramework
{
    /// <summary>   Interface for sorting algorithms. </summary>
    public abstract class SortingAlgorithmBase
    {
        private bool _enableDebugOutput;
        private bool _visualize;
        private bool _simulateDelay;
        private bool _enableRuntimeMeasurement;

        private Stopwatch _stopwatch;

        ///=================================================================================================
        /// <summary>   Gets the name. </summary>
        ///
        /// <value> The name. </value>
        ///=================================================================================================
        public abstract string Name { get; }

        ///=================================================================================================
        /// <summary>   Gets the (average) complexity. </summary>
        ///
        /// <value> The complexity. </value>
        ///=================================================================================================
        public abstract AlgorithmComplexity Complexity { get; }

        ///=================================================================================================
        /// <summary>   Gets the polynomial complexity if Complexity is set to polynomial. </summary>
        ///
        /// <value> The polynomial complexity. </value>
        ///=================================================================================================
        public virtual int? PolynomialComplexity { get; } = null;

        ///=================================================================================================
        /// <summary>   Gets or sets a value indicating whether the debugging is enabled. </summary>
        ///
        /// <value> true if enable debugging, false if not. </value>
        ///=================================================================================================
        public bool EnableDebugging { get; set; } = false;

        ///=================================================================================================
        /// <summary>   Gets a value indicating whether the debug output is enabled. </summary>
        ///
        /// <value> true if enable debug output, false if not. </value>
        ///=================================================================================================
        public bool EnableDebugOutput
        {
            get { return EnableDebugging && _enableDebugOutput; }
            set { _enableDebugOutput = value; }
        }

        ///=================================================================================================
        /// <summary>
        ///     Gets or sets a value indicating whether the sorting algorithm should be visualized.
        /// </summary>
        ///
        /// <value> true to visualize, false if not. </value>
        ///=================================================================================================
        public bool Visualize
        {
            get { return EnableDebugging && _visualize; }
            set { _visualize = value; }
        }

        ///=================================================================================================
        /// <summary>   Gets or sets a value indicating whether a delay should be simulated. </summary>
        ///
        /// <value> true if simulate delay, false if not. </value>
        ///=================================================================================================
        public bool SimulateDelay
        {
            get { return EnableDebugging && _simulateDelay; }
            set { _simulateDelay = value; }
        }

        ///=================================================================================================
        /// <summary>   Gets or sets the delay in milliseconds. </summary>
        ///
        /// <value> The delay. </value>
        ///=================================================================================================
        public int Delay { get; set; } = 1000;

        ///=================================================================================================
        /// <summary>   Gets the number of actions throughout the most recent run. </summary>
        ///
        /// <value> The number of actions. </value>
        ///=================================================================================================
        public long ActionCount { get; protected set; }

        ///=================================================================================================
        /// <summary>   Gets the number of comparisions throughout the most recent run. </summary>
        ///
        /// <value> The number of comparisions. </value>
        ///=================================================================================================
        public long ComparisionCount { get; protected set; }

        ///=================================================================================================
        /// <summary>   Gets the runtime of the most recent run. </summary>
        ///
        /// <value> The runtime. </value>
        ///=================================================================================================
        public TimeSpan Runtime { get; protected set; }

        ///=================================================================================================
        /// <summary>
        ///     Gets or sets a value indicating whether the runtime measurement is enabled.
        /// </summary>
        ///
        /// <value> true if enable runtime measurement, false if not. </value>
        ///=================================================================================================
        public bool EnableRuntimeMeasurement
        {
            get { return EnableDebugging && _enableRuntimeMeasurement; }
            set { _enableRuntimeMeasurement = value; }
        }

        ///=================================================================================================
        /// <summary>   Sorts. </summary>
        ///
        /// <param name="data">         The data. </param>
        /// <param name="sortReverse">  (Optional) true to sort reverse. </param>
        ///=================================================================================================
        public abstract void Sort(ref int[] data, bool sortReverse = false);

        /// <summary>   Increments the comparision counter if debugging is enabled. </summary>
        [Conditional("DEBUG")]
        protected void HitCondition()
        {
            if (!EnableDebugging) return;
            ComparisionCount++;
        }

        ///=================================================================================================
        /// <summary>   Increments the comparision counter by a value if debugging is enabled. </summary>
        ///
        /// <param name="count">    Number of. </param>
        ///=================================================================================================
        [Conditional("DEBUG")]
        protected void HitCondition(int count)
        {
            if (!EnableDebugging) return;
            ComparisionCount += count;
        }

        /// <summary>   Increments the action counter if debugging is enabled. </summary>
        [Conditional("DEBUG")]
        protected void HitAction()
        {
            if (!EnableDebugging) return;
            ActionCount++;
        }

        ///=================================================================================================
        /// <summary>   Increments the action counter by a value if debugging is enabled. </summary>
        ///
        /// <param name="count">    Number of. </param>
        ///=================================================================================================
        [Conditional("DEBUG")]
        protected void HitAction(int count)
        {
            if (!EnableDebugging) return;
            ActionCount += count;
        }

        /// <summary>   Increments both counters if debugging is enabled. </summary>
        [Conditional("DEBUG")]
        protected void HitBoth()
        {
            if (!EnableDebugging) return;
            ActionCount++;
            ComparisionCount++;
        }

        /// <summary>   Increments both counters by a value if debugging is enabled. </summary>
        [Conditional("DEBUG")]
        protected void HitBoth(int count)
        {
            if (!EnableDebugging) return;
            ActionCount += count;
            ComparisionCount += count;
        }

        ///=================================================================================================
        /// <summary>   Increments both counters by a value if debugging is enabled. </summary>
        ///
        /// <param name="actionCount">  The number of actions. </param>
        /// <param name="compCount">    Number of comparisions. </param>
        ///=================================================================================================
        [Conditional("DEBUG")]
        protected void HitBoth(int actionCount, int compCount)
        {
            if (!EnableDebugging) return;
            ActionCount += actionCount;
            ComparisionCount += compCount;
        }

        /// <summary>   Resets the debug data. </summary>
        protected void ResetDebugData()
        {
            ActionCount = 0;
            ComparisionCount = 0;
            Runtime = TimeSpan.Zero;
        }

        /// <summary>   Starts runtime tracking if enabled. </summary>
        protected void StartTracking()
        {
            if (!EnableRuntimeMeasurement) return;
            if (_stopwatch == null) _stopwatch = new Stopwatch();
            _stopwatch.Restart();
        }

        /// <summary>   Stops runtime tracking if enabled. </summary>
        protected void StopTracking()
        {
            if (!EnableRuntimeMeasurement) return;
            _stopwatch?.Stop();
            Runtime = _stopwatch?.Elapsed ?? TimeSpan.Zero;
        }

        ///=================================================================================================
        /// <summary>   Returns a string that represents the current object. </summary>
        ///
        /// <returns>   A string that represents the current object. </returns>
        ///
        /// <seealso cref="M:System.Object.ToString()"/>
        ///=================================================================================================
        public override string ToString()
        {
            return Name;
        }
    }
}
