﻿using System;

namespace BaseFramework
{
    /// <summary>   An int array extensions. </summary>
    public static class IntArrayExtensions
    {
        ///=================================================================================================
        /// <summary>   An int[] extension method that fills an array with random numbers. </summary>
        ///
        /// <param name="array">    The array to act on. </param>
        ///
        /// <returns>   An int[]. </returns>
        ///=================================================================================================
        public static int[] FillRandom(this int[] array)
        {
            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
                array[i] = rnd.Next();
            return array;
        }

        ///=================================================================================================
        /// <summary>
        ///     An int[] extension method that fills an array with random numbers using an lower and
        ///     upper threshold of generated values.
        /// </summary>
        ///
        /// <param name="array">            The array to act on. </param>
        /// <param name="lowerThreshold">   The lower threshold. </param>
        /// <param name="upperThreshold">   The upper threshold. </param>
        ///
        /// <returns>   An int[]. </returns>
        ///=================================================================================================
        public static int[] FillRandom(this int[] array, int lowerThreshold, int upperThreshold)
        {
            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
                array[i] = rnd.Next(lowerThreshold, upperThreshold);
            return array;
        }

        ///=================================================================================================
        /// <summary>
        ///     An int[] extension method that fills an array with random numbers using an lower and
        ///     upper threshold of generated values.
        /// </summary>
        ///
        /// <param name="array">    The array to act on. </param>
        ///
        /// <returns>   An int[]. </returns>
        ///=================================================================================================
        public static int[,] FillRandom(this int[,] array)
        {
            return FillArray(array, null, null);
        }

        ///=================================================================================================
        /// <summary>
        ///     An int[] extension method that fills an array with random numbers using an lower and
        ///     upper threshold of generated values.
        /// </summary>
        ///
        /// <param name="array">            The array to act on. </param>
        /// <param name="lowerThreshold">   The lower threshold. </param>
        /// <param name="upperThreshold">   The upper threshold. </param>
        ///
        /// <returns>   An int[]. </returns>
        ///=================================================================================================
        public static int[,] FillRandom(this int[,] array, int? lowerThreshold, int upperThreshold)
        {
            return FillArray(array, lowerThreshold, upperThreshold);
        }

        ///=================================================================================================
        /// <summary>   Fills an 2D-array. </summary>
        ///
        /// <param name="array">            The array to act on. </param>
        /// <param name="lowerThreshold">   The lower threshold. </param>
        /// <param name="upperThreshold">   The upper threshold. </param>
        ///
        /// <returns>   An int[,]. </returns>
        ///=================================================================================================
        private static int[,] FillArray(int[,] array, int? lowerThreshold, int? upperThreshold)
        {
            Random rnd = new Random();
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    if (lowerThreshold == null && upperThreshold != null)
                        array[x, y] = rnd.Next(upperThreshold.Value);
                    else if (lowerThreshold != null && upperThreshold != null)
                        array[x, y] = rnd.Next(lowerThreshold.Value, upperThreshold.Value);
                    else array[x,y] = rnd.Next();
                }
            }

            return array;
        }

        ///=================================================================================================
        /// <summary>   An int[] extension method that copies this instance. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="array">    The array to act on. </param>
        /// <param name="source">   Another instance to copy. </param>
        ///=================================================================================================
        public static void Copy(this int[] array, int[] source)
        {
            if (array.Length != source.Length)
                throw new InvalidOperationException("Source and target array are of different sizes.");
            Array.Copy(source, array, source.Length);
        }
    }
}
