﻿using System;
using System.Linq;

namespace BaseFramework
{
    /// <summary>   An helper class which outputs arrays formatted. </summary>
    public static class ArrayOut
    {
        ///=================================================================================================
        /// <summary>   Outputs the given array. </summary>
        ///
        /// <param name="array">    The array. </param>
        /// <param name="color">    (Optional) the color. </param>
        ///=================================================================================================
        public static void Out(int[] array, ConsoleColor color = ConsoleColor.White)
        {
            int max = array.Max(i => i.ToString().Length);
            int indentVal = max + 2;
            string indent = string.Empty;
            for (int i = 0; i < indentVal; i++)
                indent += " ";

            foreach (int value in array)
            {
                int delta = max - value.ToString().Length;
                ConsoleEx.Write(delta == 0 ? value.ToString() : value.ToString().PadLeft(delta + value.ToString().Length, ' '), color);
                ConsoleEx.Write(indent);
            }
            Console.WriteLine();
        }

        ///=================================================================================================
        /// <summary>   Outputs an array in vertical order with optional index numbering. </summary>
        ///
        /// <param name="array">        The array. </param>
        /// <param name="showIndex">    (Optional) true to show, false to hide the index. </param>
        /// <param name="color">        (Optional) the color. </param>
        ///=================================================================================================
        public static void OutVertical(int[] array, bool showIndex = true, ConsoleColor color = ConsoleColor.White)
        {
            int highestIndent = array.Length.ToString().Length + 2;
            string indent = string.Empty;

            for (int i = 0; i < array.Length; i++)
            {
                if (showIndex)
                {
                    int indentDepth = i.ToString().Length;
                    if (highestIndent - indentDepth != indent.Length)
                    {
                        indent = string.Empty;
                        for (int x = 0; x < highestIndent - indentDepth; x++)
                            indent += " ";
                    }
                    Console.Write($"[{i}]:" + indent);
                }
                ConsoleEx.Write(array[i] + Environment.NewLine, color);
            }
        }

        ///=================================================================================================
        /// <summary>   Outputs an array and highlights selected indices with specific colors. </summary>
        ///
        /// <param name="array">        The array. </param>
        /// <param name="color">        (Optional) the color. </param>
        /// <param name="highlighted">  A variable-length parameters list containing highlighted. </param>
        ///=================================================================================================
        public static void OutHighlighted(int[] array, ConsoleColor color, params Tuple<int, ConsoleColor>[] highlighted)
        {
            int max = array.Max(i => i.ToString().Length);
            int indentVal = max + 2;
            string indent = string.Empty;
            for (int i = 0; i < indentVal; i++)
                indent += " ";

            for (int i = 0; i < array.Length; i++)
            {
                int value = array[i];
                Tuple<int, ConsoleColor> tuple = highlighted.FirstOrDefault(t => t.Item1 == i);

                int delta = max - value.ToString().Length;
                ConsoleEx.Write(
                    delta == 0 ? value.ToString() : value.ToString().PadLeft(delta + value.ToString().Length, ' '),
                    tuple?.Item2 ?? color);
                ConsoleEx.Write(indent);
            }
            Console.WriteLine();
        }

        ///=================================================================================================
        /// <summary>   Outputs an 2D array and highlights elements matching the rectangle. </summary>
        ///
        /// <param name="array">        The array. </param>
        /// <param name="rectangle">    The rectangle. </param>
        /// <param name="color">        (Optional) the color. </param>
        /// <param name="matchColor">   (Optional) the match color. </param>
        ///=================================================================================================
        public static void Out2D(int[,] array, Rectangle rectangle, ConsoleColor color = ConsoleColor.White,
            ConsoleColor matchColor = ConsoleColor.Magenta)
        {
            int max = Max2D(array);
            int indentVal = max.ToString().Length + 2;
            string indent = string.Empty;
            for (int i = 0; i < indentVal; i++)
                indent += " ";

            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    ConsoleEx.Write(array[x, y].ToString(), rectangle.Inside(x, y) ? matchColor : color);
                    Console.Write(indent);
                }
                Console.Write(Environment.NewLine);
            }

            Console.WriteLine();
        }

        ///=================================================================================================
        /// <summary>   Maximum of a 2d array. </summary>
        ///
        /// <param name="array">    The array. </param>
        ///
        /// <returns>   An int. </returns>
        ///=================================================================================================
        private static int Max2D(int[,] array)
        {
            int max = int.MinValue;
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    if (array[x, y] > max)
                        max = array[x, y];
                }
            }
            return max;
        }
    }
}