﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework
{
    /// <summary>   A console extension. </summary>
    public static class ConsoleEx
    {
        ///=================================================================================================
        /// <summary>   Writes a line in a certain color. </summary>
        ///
        /// <param name="message">      The message. </param>
        /// <param name="foreground">   (Optional) the foreground. </param>
        ///=================================================================================================
        public static void WriteLine(string message = "", ConsoleColor foreground = ConsoleColor.White)
        {
            if (string.IsNullOrEmpty(message))
            {
                Console.WriteLine();
                return;
            }

            ConsoleColor b = Console.ForegroundColor;
            Console.ForegroundColor = foreground;
            Console.WriteLine(message);
            Console.ForegroundColor = b;
        }

        ///=================================================================================================
        /// <summary>   Writes a specified amount of linebreaks. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        ///
        /// <param name="lineBreaks">   The line breaks. </param>
        ///=================================================================================================
        public static void WriteLine(int lineBreaks)
        {
            if (lineBreaks <= 0) throw new ArgumentOutOfRangeException(nameof(lineBreaks));
            for (int i = lineBreaks; i > 0; i--)
                Console.WriteLine();
        }

        ///=================================================================================================
        /// <summary>   Writes in a certain color. </summary>
        ///
        /// <param name="message">      The message. </param>
        /// <param name="foreground">   (Optional) the foreground. </param>
        ///=================================================================================================
        public static void Write(string message, ConsoleColor foreground = ConsoleColor.White)
        {
            ConsoleColor b = Console.ForegroundColor;
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ForegroundColor = b;
        }

        ///=================================================================================================
        /// <summary>   Writes a breakline. </summary>
        ///
        /// <param name="char">     (Optional) the character. </param>
        /// <param name="color">    (Optional) the color. </param>
        ///=================================================================================================
        public static void WriteBreakline(char @char = '-', ConsoleColor color = ConsoleColor.White)
        {
            string buffer = string.Empty;
            // - 1 to fix the additional line break
            for (int i = 0; i < Console.BufferWidth - 1; i++)
            {
                buffer += @char;
            }

            WriteLine(buffer, color);
        }

        ///=================================================================================================
        /// <summary>   Renders a progress bar. </summary>
        ///
        /// <param name="value"> The progress. </param>
        /// <param name="total">    Number of. </param>
        /// <param name="size">     (Optional) the size. </param>
        ///=================================================================================================
        public static void RenderProgress(int value, int total, int size = 32)
        {
            Console.CursorLeft = 0;
            Console.Write("[");
            Console.CursorLeft = size;
            Console.Write("]");
            Console.CursorLeft = 1;

            double percent = (double)value/total;
            int progress = (int) Math.Min(percent*(size - 2), size - 2);

            int position = 1;
            for (int i = 0; i <= progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            ////draw unfilled part
            //for (int i = position; i <= 31; i++)
            //{
            //    Console.BackgroundColor = ConsoleColor.DarkGray;
            //    Console.CursorLeft = position++;
            //    Console.Write(" ");
            //}

            //draw totals
            Console.CursorLeft = size + 3;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write($"{value} of {total} | {Math.Round(percent*100, 0)} %"); //blanks at the end remove any excess
        }

        ///=================================================================================================
        /// <summary>   Writes data formatted as table in two columns. </summary>
        ///
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        ///
        /// <typeparam name="T1">   Generic type parameter. </typeparam>
        /// <typeparam name="T2">   Generic type parameter. </typeparam>
        /// <param name="data">         The data. </param>
        /// <param name="distanceChar"> (Optional) the distance character. </param>
        /// <param name="colorMap">     (Optional) the color map. </param>
        ///=================================================================================================
        public static void WriteTabular2<T1, T2>(Tuple<T1, T2>[] data, char distanceChar = '.', ConsoleColor[,] colorMap = null)
        {
            if (colorMap != null && colorMap.GetLength(1) < data.Length)
                throw new ArgumentException("The specified color map is smaller than the provided data block.", nameof(colorMap));
            if (colorMap != null && colorMap.GetLength(0) < 2)
                throw new ArgumentException("The color map must be at least 2 fields wide.", nameof(colorMap));

            int columnStart = Console.BufferWidth/2;

            // Calculate maximum width of first column
            // Move second column to the right by (Diff + 4) to ensure optical alignment
            int maxWidth = data.Max(d => d.Item1.ToString().Length);
            if (columnStart - maxWidth <= 0)
                columnStart += Math.Abs(columnStart - maxWidth) + 4;

            for (int i = 0; i < data.Length; i++)
            {
                ConsoleColor color = colorMap?[0, i] ?? ConsoleColor.White;
                string buffer = data[i].Item1.ToString();
                Write(buffer, color);

                int dist = columnStart - buffer.Length;
                buffer = GenerateDistanceString(dist, distanceChar);
                Write(buffer, ConsoleColor.DarkGray);

                color = colorMap?[1, i] ?? ConsoleColor.White;
                buffer = data[i].Item2.ToString();
                Write(buffer, color);
                WriteLine();
            }
        }

        public static void WriteTabular3<T1, T2, T3>(Tuple<T1, T2, T3>[] data, char distanceChar = '.',
            ConsoleColor[,] colorMap = null)
        {
            if (colorMap != null && colorMap.GetLength(1) < data.Length)
                throw new ArgumentException("The specified color map is smaller than the provided data block.", nameof(colorMap));
            if (colorMap != null && colorMap.GetLength(0) < 3)
                throw new ArgumentException("The color map must be at least 3 fields wide.", nameof(colorMap));

            int columnStart = Console.BufferWidth / 3;
            
            // Calculate maximum width of first column
            // Move columns to the right by (Diff + 4) to ensure optical alignment
            int maxWidth = data.Max(d => d.Item1.ToString().Length);
            if (columnStart - maxWidth <= 0)
                columnStart += Math.Abs(columnStart - maxWidth) + 4;

            int column3Start = columnStart * 2;

            for (int i = 0; i < data.Length; i++)
            {
                int totalPreBuffer = 0;
                // Write Column 1
                ConsoleColor color = colorMap?[0, i] ?? ConsoleColor.White;
                string buffer = data[i].Item1.ToString();
                totalPreBuffer += buffer.Length;
                Write(buffer, color);

                // Write distance indicators
                int dist = columnStart - buffer.Length;
                buffer = GenerateDistanceString(dist, distanceChar);
                totalPreBuffer += buffer.Length;
                Write(buffer, ConsoleColor.DarkGray);

                // Write Column 2
                color = colorMap?[1, i] ?? ConsoleColor.White;
                buffer = data[i].Item2.ToString();
                totalPreBuffer += buffer.Length;
                Write(buffer, color);

                // Write distance indicators
                dist = column3Start - totalPreBuffer;
                buffer = GenerateDistanceString(dist, distanceChar);
                Write(buffer, ConsoleColor.DarkGray);

                // Write Column 3
                color = colorMap?[2, i] ?? ConsoleColor.White;
                buffer = data[i].Item3.ToString();
                Write(buffer, color);

                WriteLine();
            }
        }

        ///=================================================================================================
        /// <summary>   Writes a table of arbitrary size with optional header row. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="ArgumentException">            Thrown when one or more arguments have
        ///                                                 unsupported or illegal values. </exception>
        ///
        /// <param name="columns">          The columns. </param>
        /// <param name="data">             The data. </param>
        /// <param name="includeHeaderRow">   (Optional) true to include, false to exclude the header row. </param>
        /// <param name="colorMap">         (Optional) the color map. </param>
        /// <param name="distanceChar">     (Optional) the distance character. </param>
        ///=================================================================================================
        public static void WriteTabular<T>(int columns, T[][] data, bool includeHeaderRow = false, ConsoleColor[,] colorMap = null,
            char distanceChar = '.')
        {
            if (columns <= 0) throw new ArgumentOutOfRangeException(nameof(columns));
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Length == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(data));
            if (data[0].GetLength(0) < columns)
                throw new ArgumentException($"{nameof(data)} must have an x-Size of at least {columns}.", nameof(data));
            if (colorMap != null)
            {
                if (colorMap.GetLength(0) < data[0].Length)
                    throw new ArgumentException("The color map must be at least as broad as the data array.",
                        nameof(colorMap));
                if (colorMap.GetLength(1) < data.Length)
                    throw new ArgumentException("The color map must be at least as high as the data array.");
            }

            int baseColumnWidth = Console.BufferWidth / columns;

            // TODO
            //// Adjust column sizes to max
            //for (int i = 0; i < columns; i++)
            //{
            //    int max = data[0].Max(d => d.ToString().Size);

            //}

            WriteBreakline();

            for (int i = 0; i < data.Length; i++)
            {
                int preBuffer = 0;
                for (int col = 0; col < columns; col++)
                {
                    // Write Column
                    ConsoleColor color = colorMap?[col, i] ?? ConsoleColor.White;
                    string buffer = data[i][col].ToString();
                    preBuffer += buffer.Length;
                    Write(buffer, color);

                    // Write distance indicators
                    if (col == columns - 1) break;
                    int dist = baseColumnWidth*(col + 1) - preBuffer;
                    buffer = GenerateDistanceString(dist, i == 0 && includeHeaderRow ? ' ' : distanceChar);
                    preBuffer += buffer.Length;
                    Write(buffer, ConsoleColor.DarkGray);
                }

                if (i == 0 && includeHeaderRow)
                {
                    WriteLine();
                    WriteBreakline();
                    continue;
                }

                WriteLine();
            }

            WriteBreakline();
        }

        ///=================================================================================================
        /// <summary>   Makes tabular color map with different colors for header and data. </summary>
        ///
        /// <param name="data">         The data. </param>
        /// <param name="headerColor">  The header color. </param>
        /// <param name="dataColor">    The data color. </param>
        ///
        /// <returns>   A ConsoleColor[,]. </returns>
        ///=================================================================================================
        public static ConsoleColor[,] MakeTabularColorMap(Array data, ConsoleColor headerColor, ConsoleColor dataColor)
        {
            int width = data.GetLength(0);
            int height = data.GetLength(1);

            ConsoleColor[,] map = new ConsoleColor[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (y == 0)
                        map[x, y] = headerColor;
                    else map[x, y] = dataColor;
                }
            }

            return map;
        }

        ///=================================================================================================
        /// <summary>   Generates a distance string. </summary>
        ///
        /// <param name="count">    Number of. </param>
        /// <param name="char">     The character. </param>
        ///
        /// <returns>   The distance string. </returns>
        ///=================================================================================================
        private static string GenerateDistanceString(int count, char @char)
        {
            string result = string.Empty;
            for (int i = 0; i < count; i++)
                result += @char;
            return result;
        }
    }
}
