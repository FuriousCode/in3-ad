﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using BaseFramework.Visualisation;
using JetBrains.Annotations;

namespace BaseFramework.Dynamics
{
    /// <summary>
    /// A binary linked tree for type T.
    /// </summary>
    /// <typeparam name="T">The type of data this tree contains.</typeparam>
    public sealed class BinaryLinkedTree<T>
    {
        private int _visualizerId = int.MinValue;
        private BinaryLinkedTreeElement _root;
        private readonly Func<T, T, int> _comparator;

        public bool AutoVisualize { get; set; }

        public BinaryLinkedTree([NotNull] Func<T, T, int> comparatorFunc)
        {
            if (comparatorFunc == null) throw new ArgumentNullException(nameof(comparatorFunc));
            _comparator = comparatorFunc;
        }

        ~BinaryLinkedTree()
        {
            if (_visualizerId != int.MinValue)
                Visualizer.CloseWindow(_visualizerId);
            _root = null;
        }

        public void Insert(T value)
        {
            if (_root == null)
            {
                _root = new BinaryLinkedTreeElement
                {
                    Height = 0,
                    Value = value
                };

                if (AutoVisualize)
                    Visualize();
                return;
            }

            Insert(ref _root, value);
        }

        public void InsertRange(params T[] data)
        {
            foreach (T d in data)
                Insert(d);
        }

        public void Delete(T value)
        {
            
        }

        /// <summary>
        /// Prints out the tree in readable format.
        /// </summary>
        public void Print()
        {
            Print(_root);
        }

        public void Visualize()
        {
            if (_visualizerId == int.MinValue)
                _visualizerId = Visualizer.ShowWindow<BinaryLinkedTree<T>, T>("nameof(BinaryLinkedTree)<{typeof(T).Name}>",
                    GenerateVertices, GenerateEdges).Result;
            Visualizer.Visualize<BinaryLinkedTree<T>, T>(_visualizerId, this, null);
        }

        /// <summary>
        /// Prints the tree recursively through the given element.
        /// </summary>
        /// <param name="element">The element to print recursively down from.</param>
        private string Print(BinaryLinkedTreeElement element)
        {
            if (element == null) return string.Empty;

            string left = Print(element.Left);
            if (left != string.Empty)
            {
                left = left.Insert(0, "(");
                left += ")";
            }

            string right = Print(element.Right);
            if (right != string.Empty)
            {
                right = right.Insert(0, "(");
                right += ")";
            }

            return left + element.Value + right;
        }

        private void Insert(ref BinaryLinkedTreeElement element, T value)
        {
            if (element == null)
            {
                element = new BinaryLinkedTreeElement
                {
                    Height = 0,
                    Value = value
                };
                return;
            }

            if (_comparator(value, element.Value) <= 0)
            {
                Insert(ref element.Left, value);
                CheckRotationRight(ref element);
            }
            else
            {
                Insert(ref element.Right, value);
                CheckRotationLeft(ref element);
            }
        }

        /// <summary>
        /// Gets the height of any element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        private int GetHeight(BinaryLinkedTreeElement element)
        {
            if (element == null) return -1;
            return element.Height;
        }

        /// <summary>
        /// Updates the height of any element.
        /// </summary>
        /// <param name="element">The element to update.</param>
        private void UpdateHeight(BinaryLinkedTreeElement element)
        {
            element.Height = 1 + Math.Max(GetHeight(element.Left), GetHeight(element.Right));
        }

        private void RotateLeft(ref BinaryLinkedTreeElement element)
        {
            BinaryLinkedTreeElement b = element.Right;
            element.Right = b.Left;
            b.Left = element;
            element = b;

            UpdateHeight(element.Left);
            UpdateHeight(element);
        }

        private void RotateRight(ref BinaryLinkedTreeElement element)
        {
            BinaryLinkedTreeElement b = element.Left;
            element.Left = b.Right;
            b.Right = element;
            element = b;

            UpdateHeight(element.Right);
            UpdateHeight(element);
        }

        private void DoubleRotateLeft(ref BinaryLinkedTreeElement element)
        {
            RotateRight(ref element.Left);
            RotateLeft(ref element);
        }

        private void DoubleRotateRight(ref BinaryLinkedTreeElement element)
        {
            RotateLeft(ref element.Left);
            RotateRight(ref element);
        }

        private void CheckRotationRight(ref BinaryLinkedTreeElement element)
        {
            if (element == null) return;
            if (element.Left == null)
            {
                UpdateHeight(element);
                return;
            }

            if (GetHeight(element.Left) - GetHeight(element.Right) == 2)
            {
                if (GetHeight(element.Left.Right) > GetHeight(element.Left.Left))
                    DoubleRotateRight(ref element);
                else RotateRight(ref element);
            }
            else UpdateHeight(element);
        }

        private void CheckRotationLeft(ref BinaryLinkedTreeElement element)
        {
            if (element == null) return;
            if (element.Right == null)
            {
                UpdateHeight(element);
                return;
            }

            if (GetHeight(element.Right) - GetHeight(element.Left) == 2)
            {
                if (GetHeight(element.Right.Left) > GetHeight(element.Right.Right))
                    DoubleRotateLeft(ref element);
                else RotateLeft(ref element);
            }
            else UpdateHeight(element);
        }

        public static IList<GraphVertex<T>> GenerateVertices(BinaryLinkedTree<T> tree)
        {
            List<GraphVertex<T>> vertices = new List<GraphVertex<T>>();
            GenerateVertices(ref vertices, tree._root);
            return vertices;
        }

        private static void GenerateVertices(ref List<GraphVertex<T>> list, BinaryLinkedTreeElement element)
        {
            element.Vertex = new GraphVertex<T>(element.Value);
            list.Add(element.Vertex);
            if (element.Left != null)
                GenerateVertices(ref list, element.Left);
            if (element.Right != null)
                GenerateVertices(ref list, element.Right);
        }

        public static IList<GraphEdge<T>> GenerateEdges(BinaryLinkedTree<T> tree, IEnumerable<GraphVertex<T>> vertices)
        {
            List<GraphEdge<T>> edges = new List<GraphEdge<T>>();
            GenerateEdges(ref edges, tree._root, null);
            return edges;
        }

        private static void GenerateEdges(ref List<GraphEdge<T>> list, BinaryLinkedTreeElement element, BinaryLinkedTreeElement parent)
        {
            if (element.Left != null)
                GenerateEdges(ref list, element.Left, element);
            if (element.Right != null)
                GenerateEdges(ref list, element.Right, element);

            if (parent != null)
                list.Add(new GraphEdge<T>(parent.Vertex, element.Vertex));
        }

        private sealed class BinaryLinkedTreeElement
        {
            public int Height { get; set; }

            public bool Balanced => (Left?.Balanced == true && Right?.Balanced == true) || Height <= 1;

            public T Value { get; set; }

            public BinaryLinkedTreeElement Left;

            public BinaryLinkedTreeElement Right;

            public GraphVertex<T> Vertex;
        }
    }
}
