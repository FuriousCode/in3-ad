﻿using System;

namespace BaseFramework.Dynamics
{
    ///=================================================================================================
    /// <summary>   Custom implementation of a linked list. This class cannot be inherited. </summary>
    ///
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    ///
    /// <seealso cref="T:System.Collections.IEnumerable"/>
    ///=================================================================================================
    public sealed class CustomLinkedList<T>
    {
        private LinkedListElement _head;
        private LinkedListElement _tail;

        ///=================================================================================================
        /// <summary>   Gets the number of elements. </summary>
        ///
        /// <value> The count. </value>
        ///=================================================================================================
        public int Count { get; private set; }

        ///=================================================================================================
        /// <summary>   Appends a value. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public void Append(T value)
        {
            LinkedListElement element = new LinkedListElement {Value = value};

            if (_tail == null)
            {
                _head = element;
                _tail = element;
            }
            else
            {
                _tail.Next = element;
                element.Previous = _tail;
                _tail = element;
            }

            Count++;
        }

        ///=================================================================================================
        /// <summary>   Removes the given value. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public void Remove(T value)
        {
            if (_head == null) return;
            LinkedListElement current = _head;
            LinkedListElement previous = null;

            do
            {
                if (current.Value.Equals(value))
                {
                    if (previous == null)
                    {
                        _head = current.Next;
                        _head.Previous = null;
                    }
                    else
                    {
                        previous.Next = current.Next;
                        current.Next.Previous = previous;
                    }

                    if (current == _tail)
                    {
                        _tail = previous;
                        if (_tail != null)
                            _tail.Next = null;
                    }

                    current.Value = default(T);
                    current.Next = null;
                    current.Previous = null;
                    Count--;

                    break;
                }

                previous = current;
            } while ((current = current.Next) != null);
        }

        /// <summary>   Prints all elements of this list. </summary>
        public void Print()
        {
            LinkedListElement current = _head;
            do
            {
                Console.Write(current.Value.ToString());
                if (current.Next != null)
                    Console.Write("   ");
            } while ((current = current.Next) != null);
        }

        /// <summary>   Sorts this list. </summary>
        public void Sort(Func<T, T, int> comparisionFunction)
        {
            Quicksort(0, Count - 1, comparisionFunction);
        }

        ///=================================================================================================
        /// <summary>   Sorts the list using a quicksort algorithm. </summary>
        ///
        /// <param name="low">                  The low. </param>
        /// <param name="high">                 The high. </param>
        /// <param name="comparisionFunction">  The comparision function. </param>
        ///=================================================================================================
        private void Quicksort(int low, int high, Func<T, T, int> comparisionFunction)
        {
            if (low >= high) return;
            int p = Partition(low, high, comparisionFunction);
            Quicksort(low, p, comparisionFunction);
            Quicksort(p + 1, high, comparisionFunction);
        }

        ///=================================================================================================
        /// <summary>   Partitions the list. </summary>
        ///
        /// <param name="low">                  The low. </param>
        /// <param name="high">                 The high. </param>
        /// <param name="comparisionFunction">  The comparision function. </param>
        ///
        /// <returns>   An int. </returns>
        ///=================================================================================================
        private int Partition(int low, int high, Func<T, T, int> comparisionFunction)
        {
            LinkedListElement pivotElement = GetByIndex(low);
            int i = low - 1;
            int j = high + 1;

            while (true)
            {
                LinkedListElement cache1 = null;
                do
                {
                    i++;
                    cache1 = GetByIndex(i);
                } while (comparisionFunction(cache1.Value, pivotElement.Value) < 0);

                LinkedListElement cache2;
                do
                {
                    j--;
                    cache2 = GetByIndex(j);
                } while (comparisionFunction(cache2.Value, pivotElement.Value) > 0);

                if (i >= j)
                    return j;

                T temp = cache1.Value;
                cache1.Value = cache2.Value;
                cache2.Value = temp;
            }
        }

        ///=================================================================================================
        /// <summary>   Gets an element by index. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <exception cref="IndexOutOfRangeException">     Thrown when the index is outside the required
        ///                                                 range. </exception>
        ///
        /// <param name="index">    Zero-based index of the. </param>
        ///
        /// <returns>   The by index. </returns>
        ///=================================================================================================
        private LinkedListElement GetByIndex(int index)
        {
            if (index < 0) throw new ArgumentOutOfRangeException(nameof(index));
            if (index == 0) return _head;
            LinkedListElement current = _head;
            int count = 0;
            while (count < index)
            {
                current = current.Next;
                count++;

                if (current == null) break;
            }

            if (current == null)
                throw new IndexOutOfRangeException();

            return current;
        }

        /// <summary>   Clears this list and all elements. </summary>
        public void Clear()
        {
            RemoveChained(_head);
        }

        ///=================================================================================================
        /// <summary>   Deletes all elements starting from the provided one. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        private void RemoveChained(LinkedListElement value)
        {
            if (value == null) return;
            if (value.Next != null)
                RemoveChained(value.Next);
            value.Next = null;
            value.Value = default(T);
        }

        ///=================================================================================================
        /// <summary>   A linked list element. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        ///=================================================================================================
        private class LinkedListElement : ICloneable
        {
            public T Value;
            public LinkedListElement Next;
            public LinkedListElement Previous;

            ///=================================================================================================
            /// <summary>   Clones the element. </summary>
            ///
            /// <returns>   A list of. </returns>
            ///=================================================================================================
            public LinkedListElement CloneElement()
            {
                return (LinkedListElement) Clone();
            }

            ///=================================================================================================
            /// <summary>   Creates a new object that is a copy of the current instance. </summary>
            ///
            /// <returns>   A new object that is a copy of this instance. </returns>
            ///
            /// <seealso cref="M:System.ICloneable.Clone()"/>
            ///=================================================================================================
            public object Clone()
            {
                return new LinkedListElement
                {
                    Value = Value,
                    Next = Next,
                    Previous = Previous
                };
            }

            ///=================================================================================================
            /// <summary>   Returns a string that represents the current object. </summary>
            ///
            /// <returns>   A string that represents the current object. </returns>
            ///
            /// <seealso cref="M:System.Object.ToString()"/>
            ///=================================================================================================
            public override string ToString()
            {
                return Value.ToString();
            }
        }
    }
}
