﻿using System;

namespace BaseFramework.Dynamics
{
    ///=================================================================================================
    /// <summary>   List of rings. This class cannot be inherited. </summary>
    ///
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    ///=================================================================================================
    public sealed class RingList<T>
    {
        private RingListElement _head;
        private RingListElement _tail;

        ///=================================================================================================
        /// <summary>   Gets the number of elements.  </summary>
        ///
        /// <value> The count. </value>
        ///=================================================================================================
        public int Count { get; private set; }

        ///=================================================================================================
        /// <summary>   Indexer to get items within this collection using array index syntax. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <exception cref="IndexOutOfRangeException">     Thrown when the index is outside the required
        ///                                                 range. </exception>
        ///
        /// <param name="index">    Zero-based index of the entry to access. </param>
        ///
        /// <returns>   The indexed item. </returns>
        ///=================================================================================================
        public T this[int index]
        {
            get
            {
                if (index < 0) throw new ArgumentOutOfRangeException(nameof(index));
                if (index == 0) return _head.Value;
                RingListElement current = _head;
                int count = 0;
                while (count < index)
                {
                    current = current.Next;
                    count++;
                }

                if (current == null)
                    throw new IndexOutOfRangeException();

                return current.Value;
            }
        }

        ///=================================================================================================
        /// <summary>   Appends a value. </summary>
        ///
        /// <param name="value">    The value to append. </param>
        ///=================================================================================================
        public void Append(T value)
        {
            RingListElement element = new RingListElement {Value = value};
            if (_tail == null)
            {
                _head = element;
                _tail = element;
                _head.Previous = element;
                _tail.Next = element;
            }
            else
            {
                _tail.Next = element;
                element.Previous = _tail;
                _tail = element;
                _head.Previous = _tail;
                _tail.Next = _head;
            }

            Count++;
        }

        ///=================================================================================================
        /// <summary>   Removes the given value. </summary>
        ///
        /// <param name="value">    The value to append. </param>
        ///=================================================================================================
        public void Remove(T value)
        {
            if (_head == null) return;
            RingListElement current = _head;
            RingListElement previous = null;

            do
            {
                if (current.Value.Equals(value))
                {
                    if (previous == null)
                    {
                        _head = current.Next;
                        _head.Previous = _tail;
                        _tail.Next = _head;
                    }
                    else
                    {
                        previous.Next = current.Next;
                        current.Next.Previous = previous;
                    }

                    if (current == _tail)
                    {
                        _tail = previous;
                        if (_tail != null)
                            _tail.Next = _head;
                    }

                    current.Value = default(T);
                    current.Next = null;
                    current.Previous = null;
                    Count--;

                    break;
                }

                previous = current;
            } while ((current = current.Next) != _head);
        }

        ///=================================================================================================
        /// <summary>   Removes the given value. </summary>
        ///
        /// <param name="index">    Zero-based index of the. </param>
        ///=================================================================================================
        public void Remove(int index)
        {
            RingListElement value = GetByIndex(index%Count);
            value.Previous.Next = value.Next;
            value.Next.Previous = value.Previous;

            value.Previous = null;
            value.Next = null;
            value.Value = default(T);
            Count--;
        }

        ///=================================================================================================
        /// <summary>   Gets an element by index. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <exception cref="IndexOutOfRangeException">     Thrown when the index is outside the required
        ///                                                 range. </exception>
        ///
        /// <param name="index">    Zero-based index of the. </param>
        ///
        /// <returns>   The by index. </returns>
        ///=================================================================================================
        private RingListElement GetByIndex(int index)
        {
            if (index < 0) throw new ArgumentOutOfRangeException(nameof(index));
            if (index == 0) return _head;
            RingListElement current = _head;
            int count = 0;
            while (count < index)
            {
                current = current.Next;
                count++;

                if (current == _head) break;
            }

            if (current == null)
                throw new IndexOutOfRangeException();

            return current;
        }

        /// <summary>   A ring list element. This class cannot be inherited. </summary>
        private sealed class RingListElement
        {
            public T Value;
            public RingListElement Previous;
            public RingListElement Next;
        }
    }
}
