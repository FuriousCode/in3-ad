﻿using System;
using System.Collections.Generic;
using System.Security.RightsManagement;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using BaseFramework.Dynamics;
using BaseFramework.UI;

namespace BaseFramework.Visualisation
{
    /// <summary>   A graph visualizer. </summary>
    public static class Visualizer
    {
        private static readonly Dictionary<int, VisualizerWindow> Windows;
        private static int _idCounter = 0;

        /// <summary>   Static constructor. </summary>
        static Visualizer()
        {
            Windows = new Dictionary<int, VisualizerWindow>();
        }

        /// <summary>   Shows the window. </summary>
        public static async Task<int> ShowWindow<TInput, TData>(string appendix,
            VisualComposer<TInput, TData>.VertexGenerator vertexGenerator, VisualComposer<TInput, TData>.EdgeGenerator edgeGenerator)
        {
            return await Application.Current.Dispatcher.InvokeAsync(() =>
            {
                VisualizerWindow window = new VisualizerWindow();
                window.Show();
                window.SetPresentationType<TInput, TData>();
                window.SetGenerators(vertexGenerator, edgeGenerator);
                if (!string.IsNullOrEmpty(appendix))
                    window.Title += $" - {appendix}";
                Windows.Add(_idCounter, window);
                return _idCounter++;
            });
        }

        /// =================================================================================================
        ///  <summary>   Visualizes the given data with highlighted elements. </summary>
        ///     /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
        /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
        /// <param name="id">The ID of the presentation window.</param>
        /// <param name="data">         The data. </param>
        ///  <param name="highlighted">  A variable-length parameters list containing highlighted. </param>
        /// =================================================================================================
        public static void Visualize<TInput, TData>(int id, TInput data, params Tuple<TData, Color>[] highlighted)
        {
            ManualResetEvent reset = new ManualResetEvent(false);
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                VisualizerWindow window = Windows[id];
                window.UpdateGraph(data, highlighted, reset);
            });
            reset.WaitOne();
        }

        public static void CloseWindow(int id)
        {
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                VisualizerWindow window = Windows[id];
                window.Close();
                Windows.Remove(id);
            });
        }
    }
}
