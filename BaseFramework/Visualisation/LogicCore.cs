﻿using GraphX.PCL.Logic.Models;
using QuickGraph;

namespace BaseFramework.Visualisation
{
    ///=================================================================================================
    /// <summary>   A logic core. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:GraphX.PCL.Logic.Models.GXLogicCore{BaseFramework.Visualizer.GraphVertex, BaseFramework.Visualizer.GraphEdge, BaseFramework.Visualizer.BinaryGraph}"/>
    ///=================================================================================================
    internal sealed class LogicCore<T> : GXLogicCore<GraphVertex<T>, GraphEdge<T>, BidirectionalGraph<GraphVertex<T>, GraphEdge<T>>>
    {
    }
}
