﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using GraphX.Controls;
using GraphX.PCL.Common.Enums;
using GraphX.PCL.Logic.Algorithms.LayoutAlgorithms;

namespace BaseFramework.Visualisation
{
    /// <summary>
    /// A non-generic VisualComposer for graph composing.
    /// </summary>
    public abstract class VisualComposer
    {
        public object Area { get; set; }

        public abstract Type ComposerType { get; }

        public abstract Type ComposerDataType { get; }

        public abstract void SetupStyle();

        public abstract void SetupLogic();

        /// <summary>
        /// Returns this instance as type-specific VisualComposer.
        /// </summary>
        /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
        /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
        /// <returns>The generic type-specific VisualComposer.</returns>
        public VisualComposer<TInput, TData> ToSpecific<TInput, TData>()
        {
            return (VisualComposer<TInput, TData>) this;
        }
    }

    /// <summary>
    /// A generic VisualComposer for type-specific graph composing.
    /// </summary>
    /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
    /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
    public sealed class VisualComposer<TInput, TData> : VisualComposer
    {
        private VertexGenerator _vertexGenerator;
        private EdgeGenerator _edgeGenerator;

        private new BinaryTreeGraphArea<TData> Area
        {
            get { return (BinaryTreeGraphArea<TData>) base.Area; }
            set { base.Area = value; }
        }

        public override Type ComposerType => typeof(TInput);

        public override Type ComposerDataType => typeof(TData);

        public VisualComposer()
        {
            Area = new BinaryTreeGraphArea<TData>();
        }

        /// <summary>
        /// Sets the graph area's visual style.
        /// </summary>
        public override void SetupStyle()
        {
            Area.SetEdgesDashStyle(EdgeDashStyle.Solid);
            Area.ShowAllEdgesArrows();
            Area.ShowAllEdgesLabels();
            //Area.GenerateGraph(true, false);
        }

        /// <summary>
        /// Sets the generators for this VisualComposer.
        /// </summary>
        /// <param name="vertex">The vertex generator.</param>
        /// <param name="edge">The edge generator.</param>
        public void SetGenerators(VertexGenerator vertex, EdgeGenerator edge)
        {
            _vertexGenerator = vertex;
            _edgeGenerator = edge;
        }

        /// <summary>
        /// Generates the graph.
        /// </summary>
        /// <param name="data">The generic data.</param>
        /// <param name="highlighting">The elements which should be highlighted.</param>
        public void Generate(TInput data, Tuple<TData, Color>[] highlighting)
        {
            if (_vertexGenerator == null || _edgeGenerator == null)
                throw new InvalidOperationException(
                    $"The generator methods are not yet set. Call {nameof(SetGenerators)} in order to set them.");

            IList<GraphVertex<TData>> vertices = _vertexGenerator(data);
            IList<GraphEdge<TData>> edges = _edgeGenerator(data, vertices);

            // Apply highlighting
            foreach (GraphVertex<TData> vertex in vertices)
            {
                foreach (var hlElement in highlighting)
                {
                    if (hlElement.Item1.Equals(vertex.Value))
                        vertex.Color = new SolidColorBrush(hlElement.Item2);
                }
            }

            BinaryGraph<TData> graph = new BinaryGraph<TData>();
            graph.AddVertexRange(vertices);
            graph.AddEdgeRange(edges);

            Area.LogicCore.Graph = graph;
            Area.GenerateGraph();
        }
        
        /// <summary>
        /// Sets up the logic core for this composer's graph area.
        /// </summary>
        public override void SetupLogic()
        {
            LogicCore<TData> logicCore = new LogicCore<TData>
            {
                DefaultLayoutAlgorithm = LayoutAlgorithmTypeEnum.Tree
            };

            SimpleTreeLayoutParameters param;
            logicCore.DefaultLayoutAlgorithmParams = param = (SimpleTreeLayoutParameters)logicCore.AlgorithmFactory.CreateLayoutParameters(LayoutAlgorithmTypeEnum.Tree);
            //Unfortunately to change algo parameters you need to specify params type which is different for every algorithm.
            param.Direction = LayoutDirection.TopToBottom;

            //This property sets vertex overlap removal algorithm.
            //Such algorithms help to arrange vertices in the layout so no one overlaps each other.
            logicCore.DefaultOverlapRemovalAlgorithm = OverlapRemovalAlgorithmTypeEnum.FSA;
            //Default parameters are created automaticaly when new default algorithm is set and previous params were NULL
            logicCore.DefaultOverlapRemovalAlgorithmParams.HorizontalGap = 50;
            logicCore.DefaultOverlapRemovalAlgorithmParams.VerticalGap = 80;

            //This property sets edge routing algorithm that is used to build route paths according to algorithm logic.
            //For ex., SimpleER algorithm will try to set edge paths around vertices so no edge will intersect any vertex.
            //Bundling algorithm will try to tie different edges that follows same direction to a single channel making complex graphs more appealing.
            logicCore.DefaultEdgeRoutingAlgorithm = EdgeRoutingAlgorithmTypeEnum.SimpleER;

            //This property sets async algorithms computation so methods like: Area.RelayoutGraph() and Area.GenerateGraph()
            //will run async with the UI thread. Completion of the specified methods can be catched by corresponding events:
            //Area.RelayoutFinished and Area.GenerateGraphFinished.
            logicCore.AsyncAlgorithmCompute = false;

            //Finally assign logic core to GraphArea object
            Area.LogicCore = logicCore;
        }

        /// <summary>
        /// A delegate generator method for graph vertices.
        /// </summary>
        /// <param name="data">The generic data.</param>
        /// <returns>A list of vertices.</returns>
        public delegate IList<GraphVertex<TData>> VertexGenerator(TInput data);

        /// <summary>
        /// A delegate generator method for graph edges.
        /// </summary>
        /// <param name="data">The generic data.</param>
        /// <param name="vertices">The vertices which should be linked by edges.</param>
        /// <returns>A list of edges.</returns>
        public delegate IList<GraphEdge<TData>> EdgeGenerator(TInput data, IEnumerable<GraphVertex<TData>> vertices);
    }
}
