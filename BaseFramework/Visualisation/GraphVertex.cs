﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using GraphX.PCL.Common.Models;
using JetBrains.Annotations;

namespace BaseFramework.Visualisation
{
    ///=================================================================================================
    /// <summary>   A graph vertex. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:GraphX.PCL.Common.Models.VertexBase"/>
    ///=================================================================================================
    public sealed class GraphVertex<T> : VertexBase, INotifyPropertyChanged
    {
        private SolidColorBrush _color;
        private T _value;

        ///=================================================================================================
        /// <summary>   Gets or sets the text. </summary>
        ///
        /// <value> The text. </value>
        ///=================================================================================================
        public string Text => Value.ToString();

        ///=================================================================================================
        /// <summary>   Gets or sets the value. </summary>
        ///
        /// <value> The value. </value>
        ///=================================================================================================
        public T Value
        {
            get { return _value; }
            set
            {
                if (value.Equals(_value)) return;
                _value = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Text));
            }
        }

        ///=================================================================================================
        /// <summary>   Gets or sets the color. </summary>
        ///
        /// <value> The color. </value>
        ///=================================================================================================
        public SolidColorBrush Color
        {
            get { return _color; }
            set
            {
                if (Equals(value, _color)) return;
                _color = value;
                OnPropertyChanged();
            }
        }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public GraphVertex(T value)
        {
            Value = value;
            Color = Brushes.Gray;
        }

        ///=================================================================================================
        /// <summary>   Returns a string that represents the current object. </summary>
        ///
        /// <returns>   A string that represents the current object. </returns>
        ///
        /// <seealso cref="M:System.Object.ToString()"/>
        ///=================================================================================================
        public override string ToString()
        {
            return Text;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
