﻿using GraphX.PCL.Common.Models;

namespace BaseFramework.Visualisation
{
    public sealed class GraphEdge<T> : EdgeBase<GraphVertex<T>>
    {
        ///=================================================================================================
        /// <summary>   Gets or sets the text. </summary>
        ///
        /// <value> The text. </value>
        ///=================================================================================================
        public string Text { get; set; }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="source">   Source for the edge. </param>
        /// <param name="target">   Target for the edge. </param>
        /// <param name="weight">   (Optional) the weight. </param>
        ///=================================================================================================
        public GraphEdge(GraphVertex<T> source, GraphVertex<T> target, double weight = 1) : base(source, target, weight)
        {
        }

        ///=================================================================================================
        /// <summary>   Returns a string that represents the current object. </summary>
        ///
        /// <returns>   A string that represents the current object. </returns>
        ///
        /// <seealso cref="M:System.Object.ToString()"/>
        ///=================================================================================================
        public override string ToString()
        {
            return Text;
        }
    }
}
