﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace BaseFramework.Visualisation.Generators
{
    public sealed class IntArrayGenerator
    {
        public IList<GraphVertex<int>> GenerateVertices(int[] data)
        {
            return data.Select(t => new GraphVertex<int>(t)).ToList();
        }

        public IList<GraphEdge<int>> GenerateEdges(int[] data, IEnumerable<GraphVertex<int>> vertices)
        {
            var graphVertices = vertices as GraphVertex<int>[] ?? vertices.ToArray();
            List<GraphEdge<int>> edges = new List<GraphEdge<int>>();
            for (int i = 0; i < data.Length; i++)
            {
                int left = 2 * i + 1;
                int right = 2 * i + 2;

                
                if (left < graphVertices.Length)
                    edges.Add(new GraphEdge<int>(graphVertices[i], graphVertices[left]));
                if (right < graphVertices.Length)
                    edges.Add(new GraphEdge<int>(graphVertices[i], graphVertices[right]));
            }

            return edges;
        }
    }
}
