﻿using GraphX.Controls;
using QuickGraph;

namespace BaseFramework.Visualisation
{
    ///=================================================================================================
    /// <summary>   A binary tree graph area. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:GraphX.Controls.GraphArea{BaseFramework.Visualizer.GraphVertex, BaseFramework.Visualizer.GraphEdge, QuickGraph.BidirectionalGraph{BaseFramework.Visualizer.GraphVertex, BaseFramework.Visualizer.GraphEdge}}"/>
    ///=================================================================================================
    internal sealed class BinaryTreeGraphArea<T> : GraphArea<GraphVertex<T>, GraphEdge<T>, BidirectionalGraph<GraphVertex<T>, GraphEdge<T>>>
    {
    }
}
