﻿using QuickGraph;

namespace BaseFramework.Visualisation
{
    ///=================================================================================================
    /// <summary>   A binary graph. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:QuickGraph.BidirectionalGraph{BaseFramework.Visualizer.GraphVertex, BaseFramework.Visualizer.GraphEdge}"/>
    ///=================================================================================================
    internal sealed class BinaryGraph<T> : BidirectionalGraph<GraphVertex<T>, GraphEdge<T>>
    {
    }
}
