﻿using BaseFramework.Visualisation.Generators;

namespace BaseFramework.Visualisation
{
    /// <summary>
    /// A static class containing graph generator methods for various often used data types.
    /// </summary>
    public static class DefaultGenerators
    {
        /// <summary>
        /// Specifies Int[]-generator methods.
        /// </summary>
        public static IntArrayGenerator IntArray = new IntArrayGenerator();
    }
}
