﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework
{
    /// <summary>   A double array extensions. </summary>
    public static class DoubleArrayExtensions
    {
        ///=================================================================================================
        /// <summary>   A double[,] extension method that fills in random values from 0 to 'maxValue'. </summary>
        ///
        /// <param name="array">        The array to act on. </param>
        /// <param name="maxValue">    The max value. </param>
        ///=================================================================================================
        public static void FillRandom(this double[,] array, int maxValue)
        {
            Random rnd = new Random();
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    array[x, y] = rnd.NextDouble()*maxValue;
                }
            }
        }
    }
}
