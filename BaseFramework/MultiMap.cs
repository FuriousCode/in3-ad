﻿using System.Collections;
using System.Collections.Generic;

namespace BaseFramework
{
    ///=================================================================================================
    /// <summary>
    ///     A multi map which acts like a dictionary with non-unique keys. This class cannot be
    ///     inherited.
    /// </summary>
    ///
    /// <typeparam name="TKey">     Type of the key. </typeparam>
    /// <typeparam name="TValue">   Type of the value. </typeparam>
    ///=================================================================================================
    public sealed class MultiMap<TKey, TValue> : IEnumerable
    {
        private readonly Dictionary<TKey, IList<TValue>> _storage;

        /// <summary>   Default constructor. </summary>
        public MultiMap()
        {
            _storage = new Dictionary<TKey, IList<TValue>>();
        }

        ///=================================================================================================
        /// <summary>   Adds a new item. </summary>
        ///
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public void Add(TKey key, TValue value)
        {
            if (!_storage.ContainsKey(key)) _storage.Add(key, new List<TValue>());
            _storage[key].Add(value);
        }

        ///=================================================================================================
        /// <summary>   Gets the keys. </summary>
        ///
        /// <value> The keys. </value>
        ///=================================================================================================
        public IEnumerable<TKey> Keys => _storage.Keys;

        ///=================================================================================================
        /// <summary>   Queries if 'key' is contained in this MultiMap. </summary>
        ///
        /// <param name="key">  The key. </param>
        ///
        /// <returns>   true if it is contained, false if not. </returns>
        ///=================================================================================================
        public bool ContainsKey(TKey key)
        {
            return _storage.ContainsKey(key);
        }

        ///=================================================================================================
        /// <summary>   Indexer to get items within this collection using array index syntax. </summary>
        ///
        /// <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
        ///
        /// <param name="key">  The key. </param>
        ///
        /// <returns>   The indexed item. </returns>
        ///=================================================================================================
        public IList<TValue> this[TKey key]
        {
            get
            {
                if (!_storage.ContainsKey(key))
                    throw new KeyNotFoundException($"The given key {key} was not found in the collection.");
                return _storage[key];
            }
        }

        ///=================================================================================================
        /// <summary>   Returns an enumerator that iterates through a collection. </summary>
        ///
        /// <returns>
        ///     An <see cref="T:System.Collections.IEnumerator" />
        ///      object that can be used to iterate through the collection.
        /// </returns>
        ///
        /// <seealso cref="M:System.Collections.IEnumerable.GetEnumerator()"/>
        ///=================================================================================================
        public IEnumerator GetEnumerator()
        {
            return _storage.GetEnumerator();
        }
    }
}
