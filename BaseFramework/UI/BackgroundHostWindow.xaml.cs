﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Timers;
using System.Windows;
using Timer = System.Timers.Timer;

namespace BaseFramework.UI
{
    /// <summary>
    /// Interaction logic for BackgroundHostWindow.xaml
    /// </summary>
    public partial class BackgroundHostWindow
    {
        private static readonly object Lock = new object();
        private static List<Window> _windowsToActivate;
        private static Timer _timer;
        private bool _timerLock = false;

        public BackgroundHostWindow()
        {
            InitializeComponent();
            _windowsToActivate = new List<Window>();
            _timer = new Timer(500);
            _timer.Elapsed += TimerOnElapsed;
            _timer.Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _timer.Dispose();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (_windowsToActivate.Count <= 0) return;
            if (_timerLock) return;
            lock (Lock)
            {
                _timerLock = true;
                Window[] array = _windowsToActivate.ToArray();
                foreach (Window window in array)
                {
                    window.Show();
                    _windowsToActivate.Remove(window);
                }
                _timerLock = false;
            }
        }

        ///=================================================================================================
        /// <summary>   Shows the window. </summary>
        ///
        /// <param name="window">   The window. </param>
        ///=================================================================================================
        public static void ShowWindow(Window window)
        {
            lock (Lock)
            {
                if (_windowsToActivate.Contains(window)) return;
                _windowsToActivate.Add(window);
            }
        }
    }
}
