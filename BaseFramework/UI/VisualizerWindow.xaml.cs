﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using BaseFramework.Visualisation;

namespace BaseFramework.UI
{
    /// <summary>
    /// Interaction logic for VisualizerWindow.xaml
    /// </summary>
    public partial class VisualizerWindow
    {
        private readonly object Lock = new object();
        private bool _created = false;
        private VisualComposer _composer;

        public VisualizerWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Changes the presentation type for this window.
        /// </summary>
        /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
        /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
        public void SetPresentationType<TInput, TData>()
        {
            // Don't recreate an existing composer with the same type
            if (_composer?.ComposerType == typeof(TInput)) return;
            _composer = new VisualComposer<TInput, TData>();
            _composer.SetupStyle();
            _composer.SetupLogic();
            Zoom.Content = _composer.Area;
            Zoom.ViewFinder.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Sets the generators for the underlying VisualComposer.
        /// </summary>
        /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
        /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
        /// <param name="vertexGenerator">The vertex generator.</param>
        /// <param name="edgeGenerator">The edge generator.</param>
        public void SetGenerators<TInput, TData>(VisualComposer<TInput, TData>.VertexGenerator vertexGenerator,
            VisualComposer<TInput, TData>.EdgeGenerator edgeGenerator)
        {
            if (_composer == null)
                throw new InvalidOperationException(
                    $"A VisualComposer must be created first by calling {nameof(SetPresentationType)} first.");
            _composer.ToSpecific<TInput, TData>().SetGenerators(vertexGenerator, edgeGenerator);
        }

        /// =================================================================================================
        ///  <summary>   Updates the graph described by data. </summary>
        ///     /// <typeparam name="TInput">The type of input this VisualComposer should process.</typeparam>
        /// <typeparam name="TData">The type of data that is wrapped in TInput.</typeparam>
        ///  <param name="data">     The data. </param>
        /// <param name="highlighted">The highlighted data.</param>
        /// <param name="reset">    The reset. </param>
        /// =================================================================================================
        public void UpdateGraph<TInput, TData>(TInput data, Tuple<TData, Color>[] highlighted, ManualResetEvent reset)
        {
            if (!Monitor.TryEnter(Lock)) return;

            _composer.ToSpecific<TInput, TData>().Generate(data, highlighted);
            Zoom.ZoomToFill();

            Monitor.Exit(Lock);
            reset.Set();
        }
    }
}
