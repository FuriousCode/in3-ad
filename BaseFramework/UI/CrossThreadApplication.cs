﻿using System.Threading.Tasks;
using System.Windows;

namespace BaseFramework.UI
{
    ///=================================================================================================
    /// <summary>   A threaded application. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:System.Windows.Application"/>
    ///=================================================================================================
    internal sealed class CrossThreadApplication : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Task.Factory.StartNew(RunTaskManager, TaskCreationOptions.LongRunning).ContinueWith(ExitAction);

            base.OnStartup(e);
        }

        ///=================================================================================================
        /// <summary>
        ///     Continuation once the TaskManager exits. Shuts down the application completely.
        /// </summary>
        ///
        /// <param name="task"> The task. </param>
        ///=================================================================================================
        private void ExitAction(Task task)
        {
            Dispatcher.InvokeAsync(() => Shutdown(0));
        }

        /// <summary>   Executes the task manager operation. </summary>
        private void RunTaskManager()
        {
            TaskManager.ScanTasks();
            TaskManager.Run();
        }
    }
}
