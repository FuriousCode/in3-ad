﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace BaseFramework
{
    /// <summary>   Manager for tasks. This class cannot be inherited. </summary>
    public static class TaskManager
    {
        private static readonly List<TaskBase> Tasks;
        private static readonly Dictionary<int, TaskBase> KeyBindings;

        static TaskManager()
        {
            Tasks = new List<TaskBase>();
            KeyBindings = new Dictionary<int, TaskBase>();
        }

        /// <summary>   Scans the all existing tasks in the entry assembly. </summary>
        public static void ScanTasks()
        {
            DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory);
            foreach (FileInfo fi in di.GetFiles("*.dll", SearchOption.TopDirectoryOnly))
            {
                Assembly sA = Assembly.LoadFrom(fi.FullName);

                foreach (Type type in sA.GetTypes())
                {
                    if (type.IsSubclassOf(typeof(TaskBase)))
                    {
                        TaskBase taskBase = (TaskBase)Activator.CreateInstance(type);
                        Tasks.Add(taskBase);
                    }
                }
            }

            // Sort descending by Sheet, Task and Subtask number
            Tasks.Sort((a, b) =>
            {
                if (a.SheetNumber > b.SheetNumber) return 1;
                if (a.SheetNumber < b.SheetNumber) return -1;

                if (a.TaskNumber > b.TaskNumber) return 1;
                if (a.TaskNumber < b.TaskNumber) return -1;

                if (a.SubtaskNumber == null ^ b.SubtaskNumber == null)
                    throw new Exception("Two tasks with the same number must specify exact subtask numbers.");
                if (a.SubtaskNumber > b.SubtaskNumber) return 1;
                if (a.SubtaskNumber < b.SubtaskNumber) return -1;
                throw new Exception("Two tasks can't have the same task number and subtask number.");
            });
        }

        /// <summary>   Runs the task manager UI. </summary>
        public static void Run()
        {
            if (Tasks.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No tasks found. Press any button to quit.");
                Console.ReadLine();
                return;
            }

            // Generate List
            bool flag = true;
        makeList:
            GenerateList(flag);
            string result = Console.ReadLine();

            int index;
            if (!int.TryParse(result, out index))
            {
                ConsoleEx.WriteLine("Invalid input.", ConsoleColor.Red);
                Console.ReadLine();
                flag = false;
                goto makeList;
            }

            // Exit app
            if (index == -1)
                return;

            // Execute task
            if (KeyBindings.ContainsKey(index))
            {
                Console.Clear();
                TaskBase task = KeyBindings[index];

                try
                {
                    task.ExecuteTask();
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    ConsoleEx.WriteLine($"Exception occured in Sheet {task.SheetNumber} Task {task.TaskNumber}.",
                        ConsoleColor.Red);
                    ConsoleEx.WriteBreakline('-', ConsoleColor.Gray);
                    ConsoleEx.WriteLine(ex.Message + $" @ {ex.TargetSite}", ConsoleColor.DarkRed);
                    ConsoleEx.WriteLine(ex.StackTrace, ConsoleColor.Red);

                    Exception inex = ex;
                    byte count = 0;
                    while ((inex = inex.InnerException) != null)
                    {
                        ConsoleEx.WriteBreakline('=', ConsoleColor.Gray);
                        ConsoleEx.WriteLine($"[IEX {count}] {inex.Message} @ {inex.TargetSite}", ConsoleColor.Red);
                        ConsoleEx.WriteBreakline('-', ConsoleColor.Gray);
                        ConsoleEx.WriteLine(inex.StackTrace, ConsoleColor.Red);
                    }

                    ConsoleEx.WriteLine(2);
                }

                ConsoleEx.WriteLine("Press key to get back to the menu.", ConsoleColor.Yellow);
                Console.ReadLine();
            }
            else
            {
                ConsoleEx.WriteLine($"A task with index {index} does not exist.", ConsoleColor.Red);
                Console.ReadLine();
            }

            flag = false;
            goto makeList;
        }

        private static void GenerateList(bool regenerate = false)
        {
            Console.Clear();
            
            int currentSheet = Tasks[0].SheetNumber;
            ConsoleEx.WriteLine($"---------- SHEET {currentSheet} ----------", ConsoleColor.Magenta);

            int currentIndex = 1;
            int currentTask = -1;
            int previousTask = -1;
            foreach (TaskBase task in Tasks)
            {
                if (task.SheetNumber != currentSheet)
                {
                    currentSheet = task.SheetNumber;
                    ConsoleEx.WriteLine($"---------- SHEET {currentSheet} ----------", ConsoleColor.Magenta);
                }

                if (regenerate && !task.NonExecutable)
                    KeyBindings.Add(currentIndex, task);
                currentTask = task.TaskNumber;

                if (task.SubtaskNumber != null)
                {
                    if (currentTask != previousTask)
                    {
                        // Render info
                        ConsoleEx.WriteLine($"[-] | Task {task.TaskNumber}: {task.DescriptiveName}");
                    }

                    // Render subtask
                    if (task.NonExecutable)
                    {
                        Console.Write($"{"",6}[-] | ");
                        if (task.Unfinished)
                            ConsoleEx.Write("[UNFINISHED] ", ConsoleColor.Red);
                        ConsoleEx.Write("[CODE ONLY] ", ConsoleColor.Cyan);
                        Console.Write($"Task {task.TaskNumber}.{task.SubtaskNumber}: {task.DescriptiveName}\n");
                    }
                    else
                    {
                        if (task.Unfinished)
                        {
                            Console.Write($"{"",6}[{currentIndex}] | ");
                            ConsoleEx.Write("[UNFINISHED] ", ConsoleColor.Red);
                            Console.Write($"Task {task.TaskNumber}.{task.SubtaskNumber}: {task.DescriptiveName}\n");
                        }
                        else ConsoleEx.WriteLine(
                            $"{"",6}[{currentIndex}] | Task {task.TaskNumber}.{task.SubtaskNumber}: {task.DescriptiveName}");
                    }
                }
                else
                {
                    // Render task
                    if (task.NonExecutable)
                    {
                        Console.Write("[-] | ");
                        if (task.Unfinished)
                            ConsoleEx.Write("[UNFINISHED] ", ConsoleColor.Red);

                        ConsoleEx.Write("[CODE ONLY] ", ConsoleColor.Cyan);
                        Console.Write($"Task {task.TaskNumber}: {task.DescriptiveName}\n");
                    }
                    else
                    {
                        if (task.Unfinished)
                        {
                            Console.Write($"[{currentIndex}] | ");
                            ConsoleEx.Write("[UNFINISHED] ", ConsoleColor.Red);
                            Console.Write($"Task {task.TaskNumber}.{task.SubtaskNumber}: {task.DescriptiveName}\n");
                        }
                        else ConsoleEx.WriteLine($"[{currentIndex}] | Task {task.TaskNumber}: {task.DescriptiveName}");
                    }
                }

                previousTask = currentTask;
                currentIndex++;
            }

            Console.WriteLine();
            Console.WriteLine();
            ConsoleEx.WriteLine("Enter the number in the brackets to choose a task. Enter -1 to exit.", ConsoleColor.Yellow);
        }
    }
}
