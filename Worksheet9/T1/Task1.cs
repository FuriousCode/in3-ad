﻿using BaseFramework;

namespace Worksheet9.T1
{
    public sealed class Task1 : TaskBase
    {
        public override int SheetNumber => 9;
        public override int TaskNumber => 1;
        public override string DescriptiveName => "Hashing Demonstration (fix-sized Hash Table with 9 entries)";

        public override void ExecuteTask()
        {
            FixedHashTable table = new FixedHashTable();
            table.AddRange(5, 28, 19, 15, 20, 33, 12, 17, 10);

            table.Print();
        }
    }
}
