﻿using System;
using BaseFramework;

namespace Worksheet9.T1
{
    /// <summary>
    /// Implements a hashing table with a fixed size of 9.
    /// </summary>
    public sealed class FixedHashTable
    {
        private int?[] _hashTable;
        private int _elements;

        public FixedHashTable()
        {
            _hashTable = new int?[9];
            _elements = 0;
        }

        public void Add(int value)
        {
            int offset = 0;
            int hash = 0;
            while (true)
            {
                hash = Hash(value, offset++);
                if (_hashTable[hash] != null)
                    continue;
                break;
            }

            _hashTable[hash] = value;
            _elements++;
        }

        public void AddRange(params int[] values)
        {
            foreach (int value in values)
                Add(value);
        }

        public void Print()
        {
            for (int i = 0; i < _hashTable.Length; i++)
            {
                ConsoleEx.Write($"[{i}]: ");
                if (_hashTable[i] == null)
                    ConsoleEx.Write("null\n", ConsoleColor.DarkGray);
                else ConsoleEx.Write(_hashTable[i].Value + "\n", ConsoleColor.Green);
            }
        }

        /// <summary>
        /// Generates the hash value out of an input value.
        /// </summary>
        /// <param name="value">The input value.</param>
        /// <param name="i">The offset value.</param>
        /// <returns>The hashed value.</returns>
        private int Hash(int value, int i)
        {
            return i == 0 ? value % 9 : (value % 9 + i) % 9;
        }
    }
}
