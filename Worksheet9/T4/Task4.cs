﻿using System;
using BaseFramework;
using BaseFramework.Hashing;

namespace Worksheet9.T4
{
    public sealed class Task4 : TaskBase
    {
        public override int SheetNumber => 9;
        public override int TaskNumber => 4;
        public override string DescriptiveName => "Implementation of a Hash Table with all 3 probing methods.";

        public override void ExecuteTask()
        {
            ConsoleEx.WriteLine("Linear probing Hash Table:", ConsoleColor.Yellow);
            ConsoleEx.WriteBreakline('-', ConsoleColor.Yellow);

            LinearHashTable linear = new LinearHashTable(11);
            linear.AddRange(10, 22, 31, 4, 15, 28, 17, 88, 59);
            linear.Print();

            ConsoleEx.WriteLine(2);
            ConsoleEx.WriteBreakline('-', ConsoleColor.Yellow);
            ConsoleEx.WriteLine("Quadratic probing Hash Table:", ConsoleColor.Yellow);
            ConsoleEx.WriteBreakline('-', ConsoleColor.Yellow);

            QuadraticHashTable quadratic = new QuadraticHashTable(11, 1, 3);
            quadratic.AddRange(10, 22, 31, 4, 15, 28, 17, 88, 59);
            quadratic.Print();

            ConsoleEx.WriteLine(2);
            ConsoleEx.WriteBreakline('-', ConsoleColor.Yellow);
            ConsoleEx.WriteLine("Double-Hashed probing Hash Table:", ConsoleColor.Yellow);
            ConsoleEx.WriteBreakline('-', ConsoleColor.Yellow);

            DoubleHashTable doubleHash = new DoubleHashTable(11, (value, size) => value,
                (value, size) => 1 + (value % (size - 1)));
            doubleHash.AddRange(10, 22, 31, 4, 15, 28, 17, 88, 59);
            doubleHash.Print();
        }
    }
}
