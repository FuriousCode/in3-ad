﻿using System;
using System.Globalization;
using BaseFramework;

namespace Worksheet9.T2
{
    public sealed class Task2 : TaskBase
    {
        public override int SheetNumber => 9;
        public override int TaskNumber => 2;
        public override string DescriptiveName => "Calculate position of certain numbers in HashTable.";

        public override void ExecuteTask()
        {
        hashInput:
            ConsoleEx.WriteLine("Please enter the size of the hash table:", ConsoleColor.Yellow);
            string input = Console.ReadLine();

            int hashTableSize;
            if (!int.TryParse(input, out hashTableSize))
            {
                Console.Clear();
                ConsoleEx.WriteLine("Invalid input. Try again.", ConsoleColor.Red);
                goto hashInput;
            }

            ConsoleEx.WriteLine(2);

        valueInput:
            ConsoleEx.WriteLine("Please enter the values whose position in the table you want to calculate (comma separated). I.e. '23,41,23':", ConsoleColor.Yellow);
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.Clear();
                ConsoleEx.WriteLine("Invalid input. Try again.", ConsoleColor.Red);
                goto valueInput;
            }

            ConsoleEx.WriteBreakline();

            string[] elements = input.Split(',');
            int[] values = new int[elements.Length];
            for (int i = 0; i < values.Length; i++)
            {
                int value;
                if (!int.TryParse(elements[i], out value))
                    ConsoleEx.WriteLine($"{elements[i]} ({i}): INVALID INPUT.", ConsoleColor.Red);
                else
                    ConsoleEx.WriteLine($"{value}: {Hash(value, hashTableSize)}", ConsoleColor.Green);
            }
        }

        private int Hash(int value, int tableSize)
        {
            double x = (Math.Sqrt(5) - 1) / 2;
            return (int) Math.Floor(tableSize * ((value * x).FloatingDigits()));
        }
    }

    internal static class FloatingHelper
    {
        public static double FloatingDigits(this double value)
        {
            var text = value.ToString(CultureInfo.InvariantCulture).TrimEnd('0');
            var decpoint = text.IndexOf('.');
            if (decpoint < 0)
                return 0;
            return double.Parse("0." + text.Substring(decpoint + 1, text.Length - decpoint - 1),
                CultureInfo.InvariantCulture.NumberFormat);
        }
    }
}
