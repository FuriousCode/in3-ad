﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseFramework;

namespace Worksheet3
{
    public sealed class Task3 : TaskBase
    {
        public override int SheetNumber => 3;
        public override int TaskNumber => 3;

        public override string DescriptiveName => "Maximal 2D subarray sum.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
        inpI:
            ConsoleEx.WriteLine("Insert 'i' (width):", ConsoleColor.Yellow);
            string input = Console.ReadLine();
            int i;
            if (!int.TryParse(input, out i))
            {
                Console.Clear();
                ConsoleEx.WriteLine("No valid number.", ConsoleColor.Red);
                goto inpI;
            }

            if (i < 1)
            {
                Console.Clear();
                ConsoleEx.WriteLine("Only natural numbers (i >= 1) allowed.", ConsoleColor.Red);
                goto inpI;
            }

        inpJ:
            ConsoleEx.WriteLine("Insert 'j' (height):", ConsoleColor.Yellow);
            input = Console.ReadLine();
            int j;
            if (!int.TryParse(input, out j))
            {
                Console.Clear();
                ConsoleEx.WriteLine("No valid number.", ConsoleColor.Red);
                goto inpJ;
            }

            if (j < 1)
            {
                Console.Clear();
                ConsoleEx.WriteLine("Only natural numbers (j >= 1) allowed.", ConsoleColor.Red);
                goto inpJ;
            }

            // Generate numbers
            int[,] data = new int[i,j];
            Random rnd = new Random();
            for (int a = 0; a < i; a++)
            {
                for (int b = 0; b < j; b++)
                {
                    data[a, b] = rnd.Next(-1000, 1000);
                }
            }

            Console.WriteLine();
            ConsoleEx.WriteLine("Numbers generated: " + i*j, ConsoleColor.Yellow);

            // Calculate max subtotal
            Rectangle rect;
            int total = MaxSubtotal2d(data, out rect);

            // Print numbers
            Console.Clear();
            ArrayOut.Out2D(data, rect);

            ConsoleEx.WriteLine("Maximal subarray sum: " + total, ConsoleColor.Cyan);
            ConsoleEx.WriteLine($"Left: {rect.Left}, Top: {rect.Top}, Right: {rect.Right}, Bottom: {rect.Bottom}.", ConsoleColor.Yellow);
        }

        ///=================================================================================================
        /// <summary>   Maximum subarray sum of an 2D array. </summary>
        ///
        /// <param name="data">         The data. </param>
        /// <param name="selection">    [out] The selection. </param>
        ///
        /// <returns>   An int. </returns>
        ///=================================================================================================
        private int MaxSubtotal2d(int[,] data, out Rectangle selection)
        {
            int total = -1;
            int left = int.MinValue;
            int right = int.MinValue;
            int top = int.MinValue;
            int bottom = int.MinValue;

            int[] t = new int[data.GetLength(1)];
            int sum;
            // Iterate from left
            for (int i = 0; i < data.GetLength(0); i++)
            {
                // Iterate to right
                for (int j = 0; j < data.GetLength(0); j++)
                {
                    for (int x = 0; x < data.GetLength(1); x++)
                        t[i] = data[j, i];

                    int start, end;
                    sum = Kadane(t, out start, out end, data.GetLength(0));

                    if (sum <= total) continue;
                    total = sum;
                    left = i;
                    right = j;
                    top = start;
                    bottom = end;
                }
            }

            selection = new Rectangle(left, top, right, bottom);
            return total;
        }

        ///=================================================================================================
        /// <summary>
        ///     Implementation of Kadane's algorithm which does not only calculate the subarray sum but
        ///     also returns start and end position of the subarray sum.
        /// </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="start">    [out] The start. </param>
        /// <param name="end">      [out] The end. </param>
        /// <param name="n">        The int to process. </param>
        ///
        /// <returns>   An int. </returns>
        ///=================================================================================================
        private int Kadane(int[] data, out int start, out int end, int n)
        {
            int sum = -1;
            int total = -1;

            start = 0;
            end = -1;
            int s = 0;

            for (int i = 0; i < n; i++)
            {
                sum += data[i];
                if (sum < 0)
                {
                    sum = 0;
                    s = i + 1;
                }
                else if (sum > total)
                {
                    total = sum;
                    start = s;
                    end = i;
                }
            }

            if (end != -1)
                return total;

            // If all numbers are negative
            total = data[0];
            start = end = 0;

            for (int i = 1; i < n; i++)
            {
                if (data[i] > total)
                {
                    total = data[i];
                    start = end = i;
                }
            }

            return total;
        }
    }
}
