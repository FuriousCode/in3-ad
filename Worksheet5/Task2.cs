﻿using System;
using BaseFramework;
using BaseFramework.Sorting;
using static BaseFramework.ConsoleEx;

namespace Worksheet5
{
    public sealed class Task2 : TaskBase
    {
        public override int SheetNumber => 5;
        public override int TaskNumber => 2;

        public override string DescriptiveName => "Demonstration of Heap Sort and Merge Sort.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            WriteLine("Enter the time-step you want to do per cycle (in ms!):", ConsoleColor.Yellow);
            string parse = Console.ReadLine();
            if (!int.TryParse(parse, out int timeStep))
            {
                WriteLine("Failed to parse the time-step. The default of 250ms will be used.", ConsoleColor.Red);
                timeStep = 250;
            }


            int[] array = {-5, 13, -32, 7, -3, 17, 23, 12, -35, 19};
            int[] copy = new int[array.Length];

            ArrayOut.OutVertical(array, true, ConsoleColor.Yellow);

            WriteBreakline();
            WriteLine("Merge Sort:", ConsoleColor.Magenta);
            WriteBreakline();

            MergeSort sort = new MergeSort() { EnableDebugging = true, EnableDebugOutput = true, Visualize = true, SimulateDelay = true, Delay = timeStep};
            Array.Copy(array, copy, array.Length);
            sort.Sort(ref copy);

            ArrayOut.OutVertical(copy, true, ConsoleColor.Green);

            WriteLine(2);

            ArrayOut.OutVertical(array, true, ConsoleColor.Yellow);

            WriteBreakline();
            WriteLine("Heap Sort:", ConsoleColor.Magenta);
            WriteLine("Largest Value", ConsoleColor.Green);
            WriteLine("Root", ConsoleColor.Red);
            WriteLine("Previous Largest", ConsoleColor.Cyan);
            WriteBreakline();

            HeapSort heap = new HeapSort() {EnableDebugging = true, EnableDebugOutput = true, Visualize = true, SimulateDelay = true, Delay = timeStep};
            Array.Copy(array, copy, array.Length);
            heap.Sort(ref copy);

            ArrayOut.OutVertical(copy, true, ConsoleColor.Green);
        }
    }
}
