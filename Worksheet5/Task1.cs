﻿using System;
using BaseFramework;

namespace Worksheet5
{
    public sealed class Task1 : TaskBase
    {
        public override int SheetNumber => 5;
        public override int TaskNumber => 1;
        
        public override string DescriptiveName => "Weird pointless algorithm.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            int[] array = new int[20];
            array.FillRandom();

            Random rnd = new Random();

            ArrayOut.OutVertical(array, true, ConsoleColor.DarkGray);
            ConsoleEx.WriteBreakline();

            int s1 = rnd.Next(0, array.Length - 1);
            int s2 = rnd.Next(0, array.Length - 1);
            int s = array[s1] + array[s2];

            ConsoleEx.WriteLine($"Running algorithm for s1({s1}): {array[s1]}; s2({s2}): {array[s2]}; s: {s}.", ConsoleColor.Yellow);
            ConsoleEx.WriteLine($"Result was: {Algorithm(array, s)}", ConsoleColor.Cyan);
        }

        public static bool Algorithm(int[] data, int s)
        {
            // Run in positive direction
            for (int i = 0; i < data.Length; i++)
            {
                // Run back from negative direction until j == i
                for (int j = data.Length - 1; j > i; j--)
                {
                    if (i == j) continue;
                    // If a(i) + a(j) == s return true
                    if (data[i] + data[j] == s) return true;
                }
            }

            return false;
        }
    }
}
