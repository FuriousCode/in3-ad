﻿using System;

namespace Worksheet6
{
    /// <summary>   A matrix multiplication helper class. </summary>
    public static class MatrixMultiplication
    {
        ///=================================================================================================
        /// <summary>   A traditional, naive matrix multiplication. </summary>
        ///
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        ///
        /// <param name="matrixA">  The matrix a. </param>
        /// <param name="matrixB">  The matrix b. </param>
        ///
        /// <returns>   An int[,]. </returns>
        ///=================================================================================================
        public static double[,] Traditional(double[,] matrixA, double[,] matrixB)
        {
            if (matrixA.GetLength(0) != matrixB.GetLength(0)
                || matrixA.GetLength(1) != matrixB.GetLength(1))
                throw new ArgumentException("Matrices must be equally sized.");
            if (matrixA.GetLength(0) != matrixA.GetLength(1))
                throw new ArgumentException("Matrices must be quadratic.");

            int width = matrixA.GetLength(0);
            int height = matrixA.GetLength(1);

            double[,] m = new double[width, height];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    m[i, j] = 0;
                    for (int k = 0; k < width; k++)
                    {
                        m[i, j] += matrixA[i, k] * matrixB[k, j];
                    }
                }
            }

            return m;
        }

        public static double[,] Enhanced(double[,] matrixA, double[,] matrixB)
        {
            return null;
        }
    }
}
