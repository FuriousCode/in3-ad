﻿using System;
using BaseFramework;
using BaseFramework.Dynamics;

namespace Worksheet6
{
    public sealed class Task3 : TaskBase
    {
        public override int SheetNumber => 6;
        public override int TaskNumber => 3;
        
        public override string DescriptiveName => "Lotto numbers.";
        
        private const int NUMBERS_TO_CHOOSE = 6;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            RingList<int> ring = new RingList<int>();

            for (int i = 1; i < 50; i++)
                ring.Append(i);

            Random rnd = new Random();
            int startPoint = rnd.Next(0, ring.Count - 1);

            object[,] result = new object[2, NUMBERS_TO_CHOOSE + 1];
            result[0, 0] = "Order";
            result[1, 0] = "Number";

            for (int i = 1; i <= NUMBERS_TO_CHOOSE; i++)
            {
                int stepSize = ring.Count/(NUMBERS_TO_CHOOSE + 1 - i);
                int index = startPoint = startPoint + stepSize + rnd.Next(-stepSize/2, stepSize/2);
                int number = ring[index];
                ring.Remove(index);

                result[0, i] = i;
                result[1, i] = number;
            }

            ConsoleEx.WriteLine("The lotto numbers are:", ConsoleColor.Yellow);
            ConsoleEx.WriteTabular(2, result.ToDoubleArray(), true,
                ConsoleEx.MakeTabularColorMap(result, ConsoleColor.Magenta, ConsoleColor.Cyan), ' ');
        }
    }
}
