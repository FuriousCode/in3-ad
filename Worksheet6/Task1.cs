﻿using System;
using System.Collections.Generic;
using System.Linq;
using BaseFramework;
using BaseFramework.Sorting;
using MoreLinq;

namespace Worksheet6
{
    public sealed class Task1 : TaskBase
    {
        public override int SheetNumber => 6;
        public override int TaskNumber => 1;
        
        public override string DescriptiveName => "Count Sort, Heap Sort and Map Sort in comparision (with measurement).";
        
        private const int MAX_PASSES = 8;
        private const int LOWER_THRESHOLD = 1000;
        private const int HIGHER_THRESHOLD = 10000;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            int arraySize = 100;
            MultiMap<int, MeasurementResult> results = new MultiMap<int, MeasurementResult>();

            CountSort csort = new CountSort {EnableDebugging = true, EnableRuntimeMeasurement = true};
            HeapSort hsort = new HeapSort {EnableDebugging = true, EnableRuntimeMeasurement = true};
            MapSort msort = new MapSort {EnableDebugging = true, EnableRuntimeMeasurement = true};

            for (int i = 1; i < MAX_PASSES + 1; i++)
            {
                ConsoleEx.WriteLine($"Running pass {i} with an array size of {arraySize}.", ConsoleColor.Magenta);
                int[] array = new int[arraySize];
                int[] copy = new int[array.Length];
                array.FillRandom(LOWER_THRESHOLD, HIGHER_THRESHOLD);

                // Run Count Sort
                ConsoleEx.WriteLine("Running Count Sort algorithm ...", ConsoleColor.Yellow);
                copy.Copy(array);
                csort.Sort(ref copy);
                results.Add(arraySize, new MeasurementResult(csort, csort.Runtime));
                ConsoleEx.WriteLine(
                    $"Count Sort took {Math.Round(csort.Runtime.TotalSeconds, 2)} s. Actions: {csort.ActionCount} " +
                    $"| Comparisions: {csort.ComparisionCount}", ConsoleColor.Cyan);

                // Run Heap Sort
                ConsoleEx.WriteLine("Running Heap Sort algorithm ...", ConsoleColor.Yellow);
                copy.Copy(array);
                hsort.Sort(ref copy);
                results.Add(arraySize, new MeasurementResult(hsort, hsort.Runtime));
                ConsoleEx.WriteLine(
                    $"Heap Sort took {Math.Round(hsort.Runtime.TotalSeconds, 2)} s. Actions: {hsort.ActionCount} " +
                    $"| Comparisions: {hsort.ComparisionCount}", ConsoleColor.Cyan);

                // Run Map Sort
                ConsoleEx.WriteLine("Running Map Sort algorithm ...", ConsoleColor.Yellow);
                copy.Copy(array);
                msort.Sort(ref copy);
                results.Add(arraySize, new MeasurementResult(msort, msort.Runtime));
                ConsoleEx.WriteLine(
                    $"Map Sort took {Math.Round(msort.Runtime.TotalSeconds, 2)} s. Actions: {msort.ActionCount} " +
                    $"| Comparisions: {msort.ComparisionCount}", ConsoleColor.Cyan);

                if (i == MAX_PASSES) break;
                Console.WriteLine();
                Console.WriteLine();
                ConsoleEx.WriteLine(
                    $"Finished run {i}. Increasing arraySize from {arraySize} to {arraySize = arraySize << 2}.",
                    ConsoleColor.Yellow);
                ConsoleEx.WriteBreakline();
            }

            ConsoleEx.WriteLine(3);
            ConsoleEx.WriteLine("Calculating results ...", ConsoleColor.Yellow);

            object[,] mapping = new object[5, MAX_PASSES + 1];
            ConsoleColor[,] colorMap = new ConsoleColor[5, MAX_PASSES + 1];

            mapping[0, 0] = "Array Size";
            mapping[1, 0] = "Algorithm";
            mapping[2, 0] = "Runtime";
            mapping[3, 0] = "Actions";
            mapping[4, 0] = "Comparisions";

            colorMap[0, 0] = ConsoleColor.Cyan;
            colorMap[1, 0] = ConsoleColor.Cyan;
            colorMap[2, 0] = ConsoleColor.Cyan;
            colorMap[3, 0] = ConsoleColor.Cyan;
            colorMap[4, 0] = ConsoleColor.Cyan;

            int[] runs = results.Keys.ToArray();
            for (int i = 1; i < MAX_PASSES + 1; i++)
            {
                int run = runs[i - 1];
                MeasurementResult result = results[run].MinBy(r => r.Runtime);
                mapping[0, i] = run;
                mapping[1, i] = result.Algorithm.Name;
                mapping[2, i] = result.Runtime;
                mapping[3, i] = result.ActionCount;
                mapping[4, i] = result.ComparisionCount;

                colorMap[0, i] = ConsoleColor.Magenta;
                colorMap[1, i] = ConsoleColor.Magenta;
                colorMap[2, i] = ConsoleColor.Magenta;
                colorMap[3, i] = ConsoleColor.Magenta;
                colorMap[4, i] = ConsoleColor.Magenta;
            }

            ConsoleEx.WriteTabular(mapping.GetLength(0), mapping.ToDoubleArray(), true, colorMap);
        }

        ///=================================================================================================
        /// <summary>   Encapsulates the result of a measurement. </summary>
        ///
        /// <seealso cref="T:BaseFramework.TaskBase"/>
        ///=================================================================================================
        private struct MeasurementResult
        {
            public readonly SortingAlgorithmBase Algorithm;
            public readonly TimeSpan Runtime;
            public readonly long ActionCount;
            public readonly long ComparisionCount;

            ///=================================================================================================
            /// <summary>   Constructor. </summary>
            ///
            /// <param name="algorithm">    The algorithm. </param>
            /// <param name="runtime">      The runtime. </param>
            ///=================================================================================================
            public MeasurementResult(SortingAlgorithmBase algorithm, TimeSpan runtime)
            {
                Algorithm = algorithm;
                Runtime = runtime;
                ActionCount = algorithm.ActionCount;
                ComparisionCount = algorithm.ComparisionCount;
            }
        }
    }
}
