﻿using System;
using System.Diagnostics;
using BaseFramework;
using BaseFramework.Dynamics;

namespace Worksheet6
{
    public sealed class Task2 : TaskBase
    {
        public override int SheetNumber => 6;
        public override int TaskNumber => 2;
        
        public override string DescriptiveName => "Implementation of a Linked List with Quicksort";
        
        private const int ITEM_COUNT = 20000;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            ConsoleEx.WriteLine($"Creating list with {ITEM_COUNT} random values ...", ConsoleColor.Yellow);

            CustomLinkedList<int> list = new CustomLinkedList<int>();
            Random rnd = new Random();
            for (int i = 0; i < ITEM_COUNT; i++)
                list.Append(rnd.Next(0, 10000));

            //Console.WriteLine();
            //list.Print();
            //ConsoleEx.WriteLine(2);

            ConsoleEx.WriteLine("Sorting list from low to high...");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            try
            {
                list.Sort((i, i1) =>
                {
                    if (i < i1) return -1;
                    if (i == i1) return 0;
                    return 1;
                });
            }
            catch
            {
                Debugger.Break();
            }
            watch.Stop();

            //ConsoleEx.WriteLine(2);
            //Console.ForegroundColor = ConsoleColor.Cyan;
            //list.Print();
            //Console.WriteLine();

            ConsoleEx.WriteBreakline();
            ConsoleEx.WriteLine($"Sorting the list took {Math.Round(watch.Elapsed.TotalSeconds, 4)} s.", ConsoleColor.Cyan);
        }
    }
}
