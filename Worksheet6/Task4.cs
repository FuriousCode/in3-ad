﻿using System;
using System.Diagnostics;
using BaseFramework;

namespace Worksheet6
{
    public sealed class Task4 : TaskBase
    {
        public override int SheetNumber => 6;
        public override int TaskNumber => 4;
        
        public override string DescriptiveName => "Matrix multiplication (unfinished).";
        
        private const int MAX_RUNS = 250;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            int n = 2;
            Stopwatch stopwatch = new Stopwatch();

            for (int i = 1; i <= MAX_RUNS; i++)
            {
                ConsoleEx.WriteLine($"Performing run {i} for n = {n} ...", ConsoleColor.Yellow);
                ConsoleEx.WriteBreakline();

                double[,] matrixA = new double[n, n];
                double[,] matrixB = new double[n, n];

                matrixA.FillRandom(100);
                matrixB.FillRandom(100);

                stopwatch.Restart();
                double[,] result = MatrixMultiplication.Traditional(matrixA, matrixB);
                stopwatch.Stop();

                ConsoleEx.WriteLine($"Traditional method took {Math.Round(stopwatch.Elapsed.TotalSeconds, 4)} s.", ConsoleColor.Cyan);
                ConsoleEx.WriteLine(2);

                stopwatch.Restart();
                result = MatrixMultiplication.Enhanced(matrixA, matrixB);
                stopwatch.Stop();

                ConsoleEx.WriteLine($"Enhanced method took {Math.Round(stopwatch.Elapsed.TotalSeconds, 4)} s.", ConsoleColor.Cyan);
                ConsoleEx.WriteBreakline();
                n++;
            }

            // Calculate results
        }
    }
}
