﻿using System;
using System.Collections.Generic;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task3 : TaskBase
    {
        public override int SheetNumber => 2;
        public override int TaskNumber => 3;
        
        public override string DescriptiveName => "Non-recursive implementation of a method.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            Console.WriteLine(Iterative(2, 3));
        }

        public int Recursive(int n, int m)
        {
            if (n < 0) throw new ArgumentOutOfRangeException(nameof(n));
            if (m < 0) throw new ArgumentOutOfRangeException(nameof(m));

            if (n == 0) return m + 1;
            if (m == 0 & n >= 1) return Recursive(n - 1, 1);
            return Recursive(n - 1, Recursive(n, m - 1));
        }

        public int Iterative(int n, int m)
        {
            if (n < 0) throw new ArgumentOutOfRangeException(nameof(n));
            if (m < 0) throw new ArgumentOutOfRangeException(nameof(m));

            Stack<int> stack = new Stack<int>();
            while (true)
            {
                if (n == 0)
                {
                    if (stack.Count == 0)
                        return m + 1;

                    n = stack.Pop();
                    m = m + 1;
                    continue;
                }

                if (m == 0 && n >= 1)
                {
                    n--;
                    m = 1;
                    continue;
                }

                m--;
                stack.Push(n - 1);
            }
        }
    }
}
