﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD_I3.RegisterMachine
{
    /// <summary>   Values that represent registers. </summary>
    public enum Registers
    {
        CommandRegister = 0,
        AccumRegister = 1
    }
}
