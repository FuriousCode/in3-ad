﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD_I3.RegisterMachine
{
    /// <summary>   Values that represent commands. </summary>
    public enum Commands
    {
        ADD = 0x01,
        SUB = 0x02,
        MUL = 0x03,
        DIV = 0x04,
        LDA = 0x05,
        LDK = 0x06,
        STA = 0x07,
        INP = 0x08,
        OUT = 0x09,
        HLT = 0x0A,
        JMP = 0x0B,
        JEZ = 0x0C,
        JNE = 0x0D,
        JLZ = 0x0E,
        JLE = 0x0F,
        JGZ = 0x10,
        JGE = 0x11
    }
}
