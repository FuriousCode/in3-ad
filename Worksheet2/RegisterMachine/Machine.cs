﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BaseFramework;

namespace AD_I3.RegisterMachine
{
    /// <summary>   A register machine. This class cannot be inherited. </summary>
    public sealed class Machine
    {
        private byte[] _memory;
        private short[] _pMemory;

        private readonly short[] _registers;
        private int _pc;

        ///=================================================================================================
        /// <summary>   Gets or sets a value indicating whether to simulate delay. </summary>
        ///
        /// <value> true if simulate delay, false if not. </value>
        ///=================================================================================================
        public bool SimulateDelay { get; set; } = true;

        ///=================================================================================================
        /// <summary>   Gets or sets the simulation speed (command per second). </summary>
        ///
        /// <value> The simulation speed. </value>
        ///=================================================================================================
        public double SimulationSpeed { get; set; } = 0.5;

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="memoryCapacity">   (Optional) the memory capacity. </param>
        /// <param name="programMemory">    (Optional) the program memory capacity. </param>
        ///=================================================================================================
        public Machine(int memoryCapacity = 512, int programMemory = 1024)
        {
            SetupMemory(memoryCapacity, programMemory);

            int regCount = Enum.GetValues(typeof(Registers)).GetLength(0);
            _registers = new short[regCount];
        }

        ///=================================================================================================
        /// <summary>   Reads a string input. </summary>
        ///
        /// <param name="input">    The input to read. </param>
        ///=================================================================================================
        public void Load(string input)
        {
            using (MemoryStream mem = new MemoryStream(File.ReadAllBytes(input)))
                Load(mem);
        }

        ///=================================================================================================
        /// <summary>   Reads input from a stream. </summary>
        ///
        /// <param name="stream">   The stream to read. </param>
        ///=================================================================================================
        public void Load(Stream stream)
        {
            try
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    int offset = 0;
                    while (reader.Peek() >= 0)
                    {
                        string line = reader.ReadLine();
                        if (string.IsNullOrEmpty(line)) continue;
                        if (line.StartsWith("//")) continue;

                        byte[] data = StringToByteArray(line);

                        for (int i = 0; i < data.Length; i += 2)
                        {
                            byte hB = data[i];
                            byte lB = data[i + 1];
                            _pMemory[offset] = ToShort(hB, lB);
                            offset++;
                        }
                    }

                }
            }
            catch (InvalidCastException)
            {
                throw new ArgumentException("The input data is too long.", nameof(stream));
            }
        }

        ///=================================================================================================
        /// <summary>   String to byte array. </summary>
        ///
        /// <param name="hex">  The hexadecimal. </param>
        ///
        /// <returns>   A byte[]. </returns>
        ///=================================================================================================
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        /// <summary>   Updates the register information. </summary>
        private void UpdateRegisterInfo()
        {
            // Calculate max lenghts
            int cValues = _registers.Max(reg => reg);

            int prevX = Console.CursorLeft;
            int prevY = Console.CursorTop;
            int totalOffset = Console.BufferWidth - (8 + cValues.ToString().Length);

            Console.SetCursorPosition(totalOffset, Console.WindowTop + 0);
            ConsoleEx.Write($"CR: {_registers[(int) Registers.CommandRegister],5}", ConsoleColor.Magenta);

            Console.SetCursorPosition(totalOffset, Console.WindowTop + 1);
            ConsoleEx.Write($"AK: {_registers[(int) Registers.AccumRegister],5}", ConsoleColor.Magenta);

            Console.SetCursorPosition(prevX, prevY);
        }

        /// <summary>   Runs the current program. </summary>
        public void Run()
        {
            _pc = 0;

            // Jump flag
            bool jFlag = false;

            UpdateRegisterInfo();

            while (_pMemory[_pc] != 0)
            {
                // Set back jump flag
                jFlag = false;

                // Simulates loading opcode and operand into the command register
                int commandRegister = (int) Registers.CommandRegister;
                _registers[commandRegister] = _pMemory[_pc];

                // fetch opcode and operand
                byte opcode, operand;
                FromShort(_registers[commandRegister], out opcode, out operand);

                int akkuRegister = (int) Registers.AccumRegister;

                switch ((Commands)opcode)
                {
                    case Commands.ADD:
                        _registers[akkuRegister] = (short)(_registers[akkuRegister] + _memory[operand]);
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.SUB:
                        _registers[akkuRegister] = (short)(_registers[akkuRegister] - _memory[operand]);
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.MUL:
                        _registers[akkuRegister] = (short)(_registers[akkuRegister] * _memory[operand]);
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.DIV:
                        _registers[akkuRegister] = (short)(_registers[akkuRegister] / _memory[operand]);
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.LDA:
                        _registers[akkuRegister] = _memory[operand];
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.LDK:
                        _registers[akkuRegister] = operand;
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.STA:
                        _memory[operand] = (byte)_registers[akkuRegister];
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.INP:
                        ConsoleEx.WriteLine("Please input your value (byte):", ConsoleColor.Yellow);
                        string input = Console.ReadLine();
                        byte value;
                        if (!byte.TryParse(input, out value))
                            throw new Exception("Invalid input. Byte expected.");
                        _memory[operand] = value;
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.OUT:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        ConsoleEx.WriteLine(_memory[operand].ToString(), ConsoleColor.Cyan);
                        break;
                    case Commands.HLT:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        ConsoleEx.WriteLine("Program terminated.", ConsoleColor.Cyan);
                        return;
                    case Commands.JMP:
                        _pc = operand - 1;
                        jFlag = true;
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        break;
                    case Commands.JEZ:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] == 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    case Commands.JNE:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] != 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    case Commands.JLZ:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] < 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    case Commands.JLE:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] <= 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    case Commands.JGZ:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] > 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    case Commands.JGE:
                        ConsoleEx.WriteLine($"{(Commands)opcode} {operand}", ConsoleColor.Gray);
                        if (_registers[akkuRegister] >= 0)
                        {
                            _pc = operand - 1;
                            jFlag = true;
                        }
                        break;
                    default:
                        // Do nothing since the rest of the program memory could be empty (0)
                        ConsoleEx.WriteLine("Unknown opcode.", ConsoleColor.Red);
                        break;
                }

                // Only increment program counter if not jumped
                if (!jFlag)
                    _pc++;

                UpdateRegisterInfo();

                if (SimulateDelay)
                    Task.Delay(TimeSpan.FromSeconds(SimulationSpeed)).Wait();
            }
        }
        
        ///=================================================================================================
        /// <summary>   Sets up the memory. </summary>
        ///
        /// <param name="capacity">                 The capacity. </param>
        /// <param name="programMemoryCapacity">    The program memory capacity. </param>
        ///=================================================================================================
        private void SetupMemory(int capacity, int programMemoryCapacity)
        {
            _memory = new byte[capacity];
            _pMemory = new short[programMemoryCapacity];
        }

        ///=================================================================================================
        /// <summary>   Converts two bytes to a short. </summary>
        ///
        /// <param name="byte1">    The first byte. </param>
        /// <param name="byte2">    The second byte. </param>
        ///
        /// <returns>   The given data converted to a short. </returns>
        ///=================================================================================================
        private static short ToShort(short byte1, short byte2)
        {
            return (short)((byte2 << 8) + byte1);
        }

        ///=================================================================================================
        /// <summary>   Converts a short into two bytes. </summary>
        ///
        /// <param name="number">   Number of. </param>
        /// <param name="byte1">    [out] The first byte. </param>
        /// <param name="byte2">    [out] The second byte. </param>
        ///=================================================================================================
        private static void FromShort(short number, out byte byte1, out byte byte2)
        {
            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number & 255);
        }
    }
}