﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AD_I3.Machine.Utils;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task1_2 : TaskBase
    {
        public override int SheetNumber => 2;
        public override int TaskNumber => 1;
        public override int? SubtaskNumber => 2;
        public override string DescriptiveName => "Simulated random access machine.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            input:
            ConsoleEx.WriteLine("Enter the path to your script file now (leave empty to exit):", ConsoleColor.Yellow);
            string path = Console.ReadLine();

            if (string.IsNullOrEmpty(path))
                return;

            RegisterMachine.Machine machine = new RegisterMachine.Machine();
            machine.SimulationSpeed = 0.1;

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    machine.Load(fs);
                machine.Run();
            }
            catch (FileNotFoundException)
            {
                ConsoleEx.WriteLine("The file was not found. Try again.", ConsoleColor.Red);
                goto input;
            }
            catch (ParserException pex)
            {
                ConsoleEx.WriteLine(pex.ToString(), ConsoleColor.Red);
                goto input;
            }
        }
    }
}
