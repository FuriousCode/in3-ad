﻿using System;
using System.IO;
using AD_I3.Machine;
using AD_I3.Machine.Utils;
using BaseFramework;

namespace AD_I3
{
    public class Task1 : TaskBase
    {
        public override int SheetNumber => 2;
        public override int TaskNumber => 1;
        public override int? SubtaskNumber => 1;
        public override string DescriptiveName => "Simulated random access machine (INVALID - use Task 1.2).";
        public override bool NonExecutable => true;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {

            // The code provided in this exercise was invalid (wrong implementation)
            // Nonetheless kept alive since it's a simple assembler like runtime (unfinished)

        //input:
        //    ConsoleEx.WriteLine("Enter the path to your script file now (leave empty to exit):", ConsoleColor.Yellow);
        //    string path = Console.ReadLine();

        //    if (string.IsNullOrEmpty(path))
        //        return;

        //    RegisterMachine machine = new RegisterMachine(3);
        //    machine.SimulateDelay = true;
        //    machine.SimulationSpeed = 0.25;

        //    try
        //    {
        //        machine.ProcessFile(path);
        //    }
        //    catch (FileNotFoundException)
        //    {
        //        ConsoleEx.WriteLine("The file was not found. Try again.", ConsoleColor.Red);
        //        goto input;
        //    }
        //    catch (ParserException pex)
        //    {
        //        ConsoleEx.WriteLine(pex.ToString(), ConsoleColor.Red);
        //        goto input;
        //    }
        }
    }
}
