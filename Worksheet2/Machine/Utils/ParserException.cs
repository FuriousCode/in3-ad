﻿using System;

namespace AD_I3.Machine.Utils
{
    ///=================================================================================================
    /// <summary>
    ///     Exception for signalling parser errors. This class cannot be inherited.
    /// </summary>
    ///
    /// <seealso cref="T:System.Exception"/>
    ///=================================================================================================
    public sealed class ParserException : Exception
    {
        ///=================================================================================================
        /// <summary>   Gets the line number. </summary>
        ///
        /// <value> The line number. </value>
        ///=================================================================================================
        public int LineNumber { get; }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="line">     The line. </param>
        /// <param name="message">  The message. </param>
        ///=================================================================================================
        public ParserException(int line, string message) : base(message)
        {
            LineNumber = line;
        }

        ///=================================================================================================
        /// <summary>   Creates and returns a string representation of the current exception. </summary>
        ///
        /// <returns>   A string representation of the current exception. </returns>
        ///
        /// <seealso cref="M:System.Exception.ToString()"/>
        ///=================================================================================================
        public override string ToString()
        {
            return $"{LineNumber}: {Message}";
        }
    }
}
