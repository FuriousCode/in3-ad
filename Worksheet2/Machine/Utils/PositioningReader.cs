﻿using System;
using System.IO;

namespace AD_I3.Machine.Utils
{
    ///=================================================================================================
    /// <summary>
    ///     A positioning reader. Source code used from:
    ///     http://stackoverflow.com/questions/829568/how-to-know-positionlinenumber-of-a-streamreader-in-a-textfile.
    ///     Minor changes have been applied, though.
    /// </summary>
    ///
    /// <seealso cref="T:System.IO.TextReader"/>
    ///=================================================================================================
    public class PositioningReader : TextReader
    {
        private readonly TextReader _inner;

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="inner">    The inner. </param>
        ///=================================================================================================
        public PositioningReader(TextReader inner)
        {
            if (inner == null) throw new ArgumentNullException(nameof(inner));
            _inner = inner;
        }

        ///=================================================================================================
        /// <summary>
        ///     Closes the <see cref="T:System.IO.TextReader" />
        ///      and releases any system resources associated with the TextReader.
        /// </summary>
        ///
        /// <seealso cref="M:System.IO.TextReader.Close()"/>
        ///=================================================================================================
        public override void Close()
        {
            _inner.Close();
        }

        ///=================================================================================================
        /// <summary>
        ///     Reads the next character without changing the state of the reader or the character
        ///     source. Returns the next available character without actually reading it from the reader.
        /// </summary>
        ///
        /// <returns>
        ///     An integer representing the next character to be read, or -1 if no more characters are
        ///     available or the reader does not support seeking.
        /// </returns>
        ///
        /// <seealso cref="M:System.IO.TextReader.Peek()"/>
        ///=================================================================================================
        public override int Peek()
        {
            return _inner.Peek();
        }

        ///=================================================================================================
        /// <summary>
        ///     Reads the next character from the text reader and advances the character position by one
        ///     character.
        /// </summary>
        ///
        /// <returns>
        ///     The next character from the text reader, or -1 if no more characters are available. The
        ///     default implementation returns -1.
        /// </returns>
        ///
        /// <seealso cref="M:System.IO.TextReader.Read()"/>
        ///=================================================================================================
        public override int Read()
        {
            int c = _inner.Read();
            if (c >= 0)
                AdvancePosition((char)c);
            return c;
        }

        public override string ReadLine()
        {
            string r = _inner.ReadLine();
            LinePosition++;
            CharPosition += r?.Length ?? 0;
            _matched = 0;
            return r;
        }

        ///=================================================================================================
        /// <summary>   Gets the line position. </summary>
        ///
        /// <value> The line position. </value>
        ///=================================================================================================
        public int LinePosition { get; private set; }

        ///=================================================================================================
        /// <summary>   Gets the character position. </summary>
        ///
        /// <value> The character position. </value>
        ///=================================================================================================
        public int CharPosition { get; private set; }

        private int _matched;

        ///=================================================================================================
        /// <summary>   Advance position. </summary>
        ///
        /// <param name="c">    The character. </param>
        ///=================================================================================================
        private void AdvancePosition(char c)
        {
            if (Environment.NewLine[_matched] == c)
            {
                _matched++;
                if (_matched != Environment.NewLine.Length) return;
                LinePosition++;
                CharPosition = 0;
                _matched = 0;
            }
            else
            {
                _matched = 0;
                CharPosition++;
            }
        }
    }
}
