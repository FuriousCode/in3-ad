﻿namespace AD_I3.Machine
{
    /// <summary>   A command which can be executed by the register machine. This class cannot be inherited. </summary>
    internal sealed class Command
    {
        ///=================================================================================================
        /// <summary>   Gets the type. </summary>
        ///
        /// <value> The type. </value>
        ///=================================================================================================
        public CommandType Type { get; }

        ///=================================================================================================
        /// <summary>   Gets the register. </summary>
        ///
        /// <value> The register. </value>
        ///=================================================================================================
        public Register TargetRegister { get; }

        ///=================================================================================================
        /// <summary>   Gets the operator. </summary>
        ///
        /// <value> The operator. </value>
        ///=================================================================================================
        public object Operator { get; }

        ///=================================================================================================
        /// <summary>   Gets a value indicating whether the operator is a register. </summary>
        ///
        /// <value> true if operator is register, false if not. </value>
        ///=================================================================================================
        public bool OperatorRegister { get; }

        ///=================================================================================================
        /// <summary>   Gets the script. </summary>
        ///
        /// <value> The script. </value>
        ///=================================================================================================
        public string Script { get; }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="type">     The type. </param>
        /// <param name="register"> The register. </param>
        /// <param name="operator"> The operator. </param>
        /// <param name="script">   The script. </param>
        ///=================================================================================================
        internal Command(CommandType type, Register register, object @operator, string script)
        {
            Type = type;
            TargetRegister = register;
            Operator = @operator;
            OperatorRegister = @operator is Register;
            Script = script;
        }
    }
}
