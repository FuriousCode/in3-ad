﻿namespace AD_I3.Machine
{
    /// <summary>   A register for a register machine. This class cannot be inherited. </summary>
    internal sealed class Register
    {
        private int _value;

        /// <summary>   The value. </summary>
        public int Value => _value;

        ///=================================================================================================
        /// <summary>   Gets the name. </summary>
        ///
        /// <value> The name. </value>
        ///=================================================================================================
        public string Name { get; }

        ///=================================================================================================
        /// <summary>   Gets a value indicating whether this register is special. </summary>
        ///
        /// <value> true if this register is special, false if not. </value>
        ///=================================================================================================
        public bool IsSpecial { get; }

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">         The name. </param>
        /// <param name="isSpecial">    (Optional) true if this register is special. </param>
        /// <param name="initialValue"> (Optional) the initial value. </param>
        ///=================================================================================================
        public Register(string name, bool isSpecial = false, int initialValue = 0)
        {
            Name = name;
            _value = initialValue;
            IsSpecial = isSpecial;
        }

        ///=================================================================================================
        /// <summary>   Moves a constant value into this register. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public void Move(int value)
        {
            _value = value;
        }

        ///=================================================================================================
        /// <summary>   Moves another register's value into this register. </summary>
        ///
        /// <param name="register"> The register. </param>
        ///=================================================================================================
        public void Move(Register register)
        {
            _value = register.Value;
        }

        /// <summary>   Increments the value. </summary>
        public void Increment()
        {
            _value++;
        }

        /// <summary>   Decrements the value. </summary>
        public void Decrement()
        {
            _value--;
        }
    }
}
