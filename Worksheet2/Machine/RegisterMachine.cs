﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AD_I3.Machine.Utils;
using BaseFramework;

namespace AD_I3.Machine
{
    /// <summary>   A register machine. This class cannot be inherited. </summary>
    public sealed class RegisterMachine
    {
        private Register[] _registers;

        private readonly List<Command> _commandQueue;
        private readonly Dictionary<string, int> _jumpMarks;
        private int _currentPos;
        private bool _binaryMode = false;

        private bool _zFlag = false;
        private bool _sFlag = false;

        ///=================================================================================================
        /// <summary>
        ///     Gets or sets a value indicating whether the register machine should simulate a delay.
        /// </summary>
        ///
        /// <value> true if simulate delay, false if not. </value>
        ///=================================================================================================
        public bool SimulateDelay { get; set; } = true;

        ///=================================================================================================
        /// <summary>   Gets or sets the simulation speed (command per second). </summary>
        ///
        /// <value> The simulation speed. </value>
        ///=================================================================================================
        public double SimulationSpeed { get; set; } = 1.4;

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="registers">    The amount of registers. </param>
        ///=================================================================================================
        public RegisterMachine(int registers)
        {
            if (registers < 0) throw new ArgumentOutOfRangeException(nameof(registers), "Value must be greater than or equal 0.");

            SetupRegisters(registers);

            _commandQueue = new List<Command>();
            _jumpMarks = new Dictionary<string, int>();
        }

        ///=================================================================================================
        /// <summary>   Process the script described by script. </summary>
        ///
        /// <param name="script">   The script. </param>
        ///=================================================================================================
        public void ProcessScript(string script)
        {
            using (StringReader fs = new StringReader(script))
            {
                Process(fs);
            }
        }

        ///=================================================================================================
        /// <summary>   Process the file described by path. </summary>
        ///
        /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
        ///
        /// <param name="path"> Full pathname of the file. </param>
        ///=================================================================================================
        public void ProcessFile(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException("Can't locate the script file.", path);

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    Process(reader);
                }
            }
        }

        /// <summary>   Updates the register information. </summary>
        private void UpdateRegisterInfo()
        {
                // Calculate max lenghts
                int cName = _registers.Max(reg => reg.Name.Length);
                int cValues = _registers.Max(reg => reg.Value);

                int prevX = Console.CursorLeft;
                int prevY = Console.CursorTop;
                int totalOffset = Console.BufferWidth - (cName + 6 + cValues.ToString().Length);

                for (int i = 0; i < _registers.Length; i++)
                {
                    Console.SetCursorPosition(totalOffset, Console.WindowTop + i);
                    ConsoleEx.Write($"{_registers[i].Name}: {_registers[i].Value,5}", ConsoleColor.Magenta);
                }

                Console.SetCursorPosition(prevX, prevY);
        }

        private void Process(TextReader reader)
        {
            string[] supportedCommands = Enum.GetNames(typeof(CommandType));

            using (PositioningReader r = new PositioningReader(reader))
            {
                int cCom = 0;
                while (r.Peek() >= 0)
                {
                    string line = reader.ReadLine();
                    if (String.IsNullOrEmpty(line)) continue;

                    // Ignore comments
                    if (line.StartsWith("//")) continue;

                    if (line.StartsWith("#"))
                    {
                        // Special options
                        // This option defines how many registers should be used (0 or more since RA, RB, RC, RD are always available)
                        if (line.StartsWith("#REGC"))
                        {
                            try
                            {
                                int value = int.Parse(line.Split(' ')[1]);
                                if (value >= 0)
                                    SetupRegisters(value);
                                else
                                    throw new ParserException(r.LinePosition,
                                        "Invalid register count. 0 or more required. (INVALID_REG_COUNT).");
                            }
                            catch (Exception ex)
                            {
                                throw new ParserException(r.LinePosition,
                                    "Failed to parse option value (INVALID_OPTION).");
                            }
                        }
                        // This option specifies that code should be read in binary form, not in plain text
                        else if (line.StartsWith("#BIN"))
                        {
                            string[] split = line.Split(' ');
                            if (string.Equals(split[1], "ON", StringComparison.InvariantCultureIgnoreCase))
                            {
                                _binaryMode = true;
                                ConsoleEx.WriteLine("Binary mode turned on.", ConsoleColor.DarkGreen);
                            }
                            else if (string.Equals(split[1], "Off", StringComparison.InvariantCultureIgnoreCase))
                            {
                                _binaryMode = false;
                                ConsoleEx.WriteLine("Binary mode turned off.", ConsoleColor.DarkGreen);
                            }
                            else
                                throw new ParserException(r.LinePosition,
                                    "Invalid option arguments (INVALID_OPTION_ARGS).");
                        }
                        else throw new ParserException(r.LinePosition, "Invalid option (INVALID_OPTION).");

                        continue;
                    }

                    // Parse jump marks
                    string[] splits = line.Split(' ');
                    if (splits.Length < 2 && splits.Length > 0 && splits[0].EndsWith(":"))
                    {
                        _jumpMarks.Add(splits[0].Replace(":",""), cCom);
                        continue;
                    }

                    // Parse type
                    string command =
                        supportedCommands.FirstOrDefault(
                            c => string.Equals(c, splits[0], StringComparison.InvariantCultureIgnoreCase));
                    if (String.IsNullOrEmpty(command))
                        throw new ParserException(r.LinePosition, "The command was invalid (UNSUPPORTED).");

                    CommandType type = (CommandType) Enum.Parse(typeof(CommandType), command);
                    object @operator = null;

                    // Parse register
                    if (String.IsNullOrEmpty(splits[1]))
                        throw new ParserException(r.LinePosition, "The target register was not specified correctly.");

                    Register reg =
                        _registers.FirstOrDefault(
                            rg => string.Equals(rg.Name, splits[1], StringComparison.InvariantCultureIgnoreCase));
                    if (reg == null)
                    {
                        if (type == CommandType.Loop || type == CommandType.Jme || type == CommandType.Jmne || type == CommandType.Jmp)
                        {
                            // Exception: Loops don't specify registers but operators
                            @operator = splits[1];
                        }
                        else 
                            throw new ParserException(r.LinePosition,
                            "The specified target register does not exist (INVALID_NAME).");
                    }

                    // If a third part exists, parse the operator
                    if (splits.Length > 2)
                    {
                        if (String.IsNullOrEmpty(splits[2]))
                            throw new ParserException(r.LinePosition, "The operator is invalid (NULL_OR_EMPTY).");

                        Register opReg =
                            _registers.FirstOrDefault(
                                rg => string.Equals(rg.Name, splits[2], StringComparison.InvariantCultureIgnoreCase));
                        if (opReg != null)
                        {
                            @operator = opReg;
                        }
                        else
                        {
                            int @const;
                            if (!int.TryParse(splits[2], out @const))
                                throw new ParserException(r.LinePosition,
                                    "The operator was invalid (INVALID_CONST).");
                            @operator = @const;
                        }
                    }

                    // Enqueue a new command
                    _commandQueue.Add(new Command(type, reg, @operator, line));
                    cCom++;
                }
            }

            Simulate();
        }

        ///=================================================================================================
        /// <summary>   Simulates the commands in the queue. </summary>
        ///
        /// <returns>   A Task. </returns>
        ///=================================================================================================
        private void Simulate()
        {
            _currentPos = 0;
            while (_currentPos < _commandQueue.Count)
            {
                // Dequeue
                Command command = _commandQueue[_currentPos];

                // Actually simulate the command
                ConsoleEx.WriteLine(command.Script, ConsoleColor.Gray);

                Register rC;
                string jumpMark;

                switch (command.Type)
                {
                    case CommandType.Nop:
                        break;
                    case CommandType.Add:
                        if (command.OperatorRegister)
                        {
                            Register op = (Register) command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value + op.Value);
                        }
                        else
                        {
                            int constant = (int) command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value + constant);
                        }
                        break;
                    case CommandType.Sub:
                        if (command.OperatorRegister)
                        {
                            Register op = (Register)command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value - op.Value);
                        }
                        else
                        {
                            int constant = (int)command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value - constant);
                        }
                        break;
                    case CommandType.Inc:
                        command.TargetRegister.Increment();
                        break;
                    case CommandType.Dec:
                        command.TargetRegister.Decrement();
                        break;
                    case CommandType.Mov:
                        if (command.OperatorRegister)
                        {
                            Register op = (Register)command.Operator;
                            command.TargetRegister.Move(op);
                        }
                        else
                        {
                            int constant = (int)command.Operator;
                            command.TargetRegister.Move(constant);
                        }
                        break;
                    case CommandType.Out:
                        ConsoleEx.WriteLine($"{command.TargetRegister.Name}: {command.TargetRegister.Value}");
                        break;
                    case CommandType.Loop:
                        Register rc = _registers[2];
                        if (rc.Value <= 0) break;
                        rc.Decrement();

                        jumpMark = (string) command.Operator;
                        if (!_jumpMarks.ContainsKey(jumpMark))
                            throw new ParserException(_currentPos,
                                $"Jump mark '{jumpMark}' is not specified (INVALID_JUMPMARK).");
                        _currentPos = _jumpMarks[jumpMark];

                        break;
                    case CommandType.Mul:
                        if (command.OperatorRegister)
                        {
                            Register op = (Register)command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value * op.Value);
                        }
                        else
                        {
                            int constant = (int)command.Operator;
                            command.TargetRegister.Move(command.TargetRegister.Value * constant);
                        }
                        break;
                    case CommandType.Jme:
                        if (_zFlag)
                        {
                            jumpMark = (string) command.Operator;
                            if (!_jumpMarks.ContainsKey(jumpMark))
                                throw new ParserException(_currentPos,
                                    $"Jump mark '{jumpMark}' is not specified (INVALID_JUMPMARK).");
                            _currentPos = _jumpMarks[jumpMark];
                        }
                        break;
                    case CommandType.Jmne:
                        if (!_zFlag)
                        {
                            jumpMark = (string) command.Operator;
                            if (!_jumpMarks.ContainsKey(jumpMark))
                                throw new ParserException(_currentPos,
                                    $"Jump mark '{jumpMark}' is not specified (INVALID_JUMPMARK).");
                            _currentPos = _jumpMarks[jumpMark];
                        }
                        break;
                    case CommandType.Cmp:
                        Register r = command.TargetRegister;
                        int c;
                        if (command.OperatorRegister)
                        {
                            Register op = (Register)command.Operator;
                            c = r.Value - op.Value;
                        }
                        else
                        {
                            int constant = (int)command.Operator;
                            c = r.Value - constant;
                        }

                        _zFlag = c == 0;
                        _sFlag = c >= 0;

                        break;
                    case CommandType.Jmp:
                        jumpMark = (string)command.Operator;
                        if (!_jumpMarks.ContainsKey(jumpMark))
                            throw new ParserException(_currentPos,
                                $"Jump mark '{jumpMark}' is not specified (INVALID_JUMPMARK).");
                        _currentPos = _jumpMarks[jumpMark];
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                UpdateRegisterInfo();

                // Simulate delay if required
                if (SimulateDelay)
                    Task.Delay(TimeSpan.FromSeconds(SimulationSpeed)).Wait();

                _currentPos++;
            }
        }

        ///=================================================================================================
        /// <summary>   Sets up the registers. </summary>
        ///
        /// <param name="count">    Number of. </param>
        ///=================================================================================================
        private void SetupRegisters(int count)
        {
            _registers = new Register[count > 0 ? count + 4 : 4];
            _registers[0] = new Register("ra", true);
            _registers[1] = new Register("rb", true);
            _registers[2] = new Register("rc", true);
            _registers[3] = new Register("rd", true);
            for (int i = 0; i < count; i++)
                _registers[i+4] = new Register($"r{i}");
        }
    }
}
