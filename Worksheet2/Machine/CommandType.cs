﻿namespace AD_I3.Machine
{
    /// <summary>   Values that represent command types. </summary>
    public enum CommandType
    {
        Nop = 0,
        Add = 0x01,
        Sub = 0x02,
        Mul = 0x03,
        Div = 0x04,
        Lda = 0x05,
        Ldk = 0x06,
        Sta = 0x07,
        Inp = 0x08,
        Out = 0x09,
        Hlt = 0x0A,
        Jmp = 0x0B,
        Jez = 0x0C,
        Jne = 0x0D,
        Jlz = 0x0E,
        Jle = 0x0F,
        Jgz = 0x10,
        Jge = 0x11,
        Inc = 0x12,
        Dec = 0x13,
        Mov = 0x14,
        Loop = 0x1A,
        Cmp = 0x15,
    }
}
