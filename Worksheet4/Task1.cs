﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseFramework;
using BaseFramework.Sorting;

namespace Worksheet4
{
    public class Task1 : TaskBase
    {
        public override int SheetNumber => 4;
        public override int TaskNumber => 1;

        public override string DescriptiveName => "Implementation of Insertion-, Bubble-, and Selection-Sort.";
        
        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            int[] array = {-5, 13, -32, 7, -3, 17, 23, 12, -35, 19};
            int[] copy = new int[array.Length];
            ArrayOut.Out(array, ConsoleColor.Yellow);

            Console.WriteLine();
            ConsoleEx.WriteLine("Insertion-Sort:", ConsoleColor.Magenta);
            ConsoleEx.WriteLine("Insertion-Sort:", ConsoleColor.Magenta);
            ConsoleEx.WriteBreakline();
            ConsoleEx.WriteLine("Start-Index", ConsoleColor.Yellow);
            ConsoleEx.WriteLine("Insertion Index", ConsoleColor.Red);
            ConsoleEx.WriteLine("Current 'i'", ConsoleColor.Magenta);
            ConsoleEx.WriteBreakline();

            // Copy array
            Array.Copy(array, copy, array.Length);

            SortingAlgorithmBase algorithm = SortingFactory.Make<InsertionSorting>(true, true, false, true, true, TimeSpan.FromSeconds(0.5));
            algorithm.Sort(ref copy);
            ArrayOut.Out(copy, ConsoleColor.DarkGreen);
            Console.WriteLine();
            ConsoleEx.WriteLine($"Time elapsed: {algorithm.Runtime.TotalMilliseconds} ms.");

            // Bubble
            Console.WriteLine();
            ConsoleEx.WriteLine("Bubble-Sort:", ConsoleColor.Magenta);
            ConsoleEx.WriteBreakline();
            ConsoleEx.WriteLine("Bubble Value", ConsoleColor.Yellow);
            ConsoleEx.WriteLine("Replacement Value", ConsoleColor.Red);
            ConsoleEx.WriteLine("Current 'i'", ConsoleColor.Magenta);
            ConsoleEx.WriteBreakline();

            Array.Copy(array, copy, array.Length);

            algorithm = SortingFactory.Make<BubbleSort>(true, true, false, true, true, TimeSpan.FromSeconds(0.5));
            algorithm.Sort(ref copy);
            ArrayOut.Out(copy, ConsoleColor.DarkGreen);
            Console.WriteLine();
            ConsoleEx.WriteLine($"Time elapsed: {algorithm.Runtime.TotalMilliseconds} ms.");

            // Selection
            Console.WriteLine();
            ConsoleEx.WriteLine("Selection-Sort:", ConsoleColor.Magenta);
            ConsoleEx.WriteBreakline();
            ConsoleEx.WriteLine("Maximum Value", ConsoleColor.Yellow);
            ConsoleEx.WriteLine("Inserted Value", ConsoleColor.Cyan);
            ConsoleEx.WriteLine("Current 'i'", ConsoleColor.Magenta);
            ConsoleEx.WriteLine("Previous 'i'", ConsoleColor.Green);
            ConsoleEx.WriteBreakline();

            Array.Copy(array, copy, array.Length);

            algorithm = SortingFactory.Make<SelectionSort>(true, true, false, true, true, TimeSpan.FromSeconds(0.5));
            algorithm.Sort(ref copy);
            ArrayOut.Out(copy, ConsoleColor.DarkGreen);
            Console.WriteLine();
            ConsoleEx.WriteLine($"Time elapsed: {algorithm.Runtime.TotalMilliseconds} ms.");
        }
    }
}
