﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;
using BaseFramework;

namespace Worksheet4
{
    public sealed class Task3 : TaskBase
    {
        public override int SheetNumber => 4;
        public override int TaskNumber => 3;
        
        public override string DescriptiveName => "Maximum data size for one-minute runtime of any sorting algorithm.";
        
        private const int BASE_COUNT = 1000;
        private const int INDETERMINABLE_BASE_COUNT = 200000;
        private const int DEFAULT_RUNS = 8;

        private DateTime startTime;

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            ConsoleEx.WriteLine("This task will determine how big an array can be sorted on the current machine " +
                                " within 1 minute.", ConsoleColor.Cyan);
            ConsoleEx.WriteLine("First select an algorithm to test on:", ConsoleColor.Green);

            List<SortingAlgorithmBase> algorithms = new List<SortingAlgorithmBase>();
            foreach (Type t in Assembly.GetAssembly(typeof(ConsoleEx)).GetTypes())
            {
                if (!t.IsSubclassOf(typeof(SortingAlgorithmBase))) continue;
                SortingAlgorithmBase a = (SortingAlgorithmBase) Activator.CreateInstance(t);
                algorithms.Add(a);
            }

            algInp:
            for (int i = 0; i < algorithms.Count; i++)
            {
                SortingAlgorithmBase algo = algorithms[i];
                ConsoleEx.WriteLine($"{i + 1}: {algo.Name}", ConsoleColor.Cyan);
            }

            int index;
            string input = Console.ReadLine();
            if (!int.TryParse(input, out index))
            {
                Console.Clear();
                ConsoleEx.WriteLine("Invalid number. Try again.");
                goto algInp;
            }

            SortingAlgorithmBase algorithm = algorithms[index - 1];

            ConsoleEx.WriteLine("Please enter an initial array size for determination of the base time (>50):",
                ConsoleColor.Yellow);
            input = Console.ReadLine();
            int count;
            if (!int.TryParse(input, out count) || count <= 50)
            {
                ConsoleEx.WriteLine($"Invalid input. Using the default value {BASE_COUNT:N0}.", ConsoleColor.Yellow);
                count = BASE_COUNT;
            }

            // Start determination process
            ConsoleEx.WriteLine("Determining base time. This might take a few seconds ...", ConsoleColor.Yellow);
            TimeSpan time = DetermineBaseTime(algorithm, count);
            ConsoleEx.WriteLine($"Base time for {count:N0} items is: {Math.Round(time.TotalSeconds, 4)} s.",
                ConsoleColor.Cyan);
            Console.WriteLine();

            // Calculate necessary amount
            int max;
            if (algorithm.Complexity == AlgorithmComplexity.LinearLogarithmic ||
                algorithm.Complexity == AlgorithmComplexity.Logarithmic)
                max = Math.Max(INDETERMINABLE_BASE_COUNT, count);
            else max = ComplexityCalculator.GetMaxFromTime(count, time, algorithm);
            ConsoleEx.WriteLine($"Maximum amount of objects: {max:N0}.", ConsoleColor.Cyan);
            Console.WriteLine();
            ConsoleEx.WriteLine("Please enter the number of runs (more result in a more precise result):",
                ConsoleColor.Yellow);
            input = Console.ReadLine();
            int runs;
            if (!int.TryParse(input, out runs) || runs < 1)
            {
                ConsoleEx.WriteLine($"Invalid value. Using the default of {DEFAULT_RUNS:N0}.", ConsoleColor.Yellow);
                runs = DEFAULT_RUNS;
            }

            Dictionary<TimeSpan, int> results = new Dictionary<TimeSpan, int>();
            // Do a test run
            for (int i = 1; i <= runs; i++)
            {
                TimeSpan comp = TimeSpan.FromMinutes(1);
                TimeSpan delta = comp - PerformRun(i, algorithm, max);

                Console.WriteLine();
                if (Math.Abs(delta.TotalSeconds) < 0.5)
                {
                    ConsoleEx.WriteLine(
                        $"Delta was: {Math.Round(delta.TotalMilliseconds, 2)} ms ({Math.Round(delta.TotalSeconds, 2)} s).",
                        ConsoleColor.Green);
                    break;
                }

                ConsoleEx.WriteLine(
                    $"Delta was: {Math.Round(delta.TotalMilliseconds, 2)} ms ({Math.Round(delta.TotalSeconds, 2)} s).",
                    Math.Abs(delta.TotalSeconds) < 2 ? ConsoleColor.Yellow : ConsoleColor.Red);

                // Calculate new maximum based on delta time and add/subtract to last maximum
                // If delta is less than two, divide by 2 or 3 for finer values (to avoid relapse)
                int absolute;
                if (algorithm.Complexity == AlgorithmComplexity.LinearLogarithmic ||
                    algorithm.Complexity == AlgorithmComplexity.Logarithmic)
                {
                    absolute = (int) (max * Math.Sqrt(1 / delta.Duration().TotalSeconds));
                }
                else absolute = ComplexityCalculator.GetMaxFromTime(count, delta, algorithm);
                if (delta.Duration().TotalSeconds <= 1)
                    absolute /= 8;
                if (delta.Duration().TotalSeconds <= 2 && delta.Duration().TotalSeconds > 1)
                    absolute /= 4;
                if (delta.Duration().TotalSeconds <= 3 && delta.Duration().TotalSeconds > 2)
                    absolute /= 2;

                results.Add(delta, max);

                if (delta.TotalSeconds < 0)
                {
                    ConsoleEx.WriteLine($"It took {Math.Round(Math.Abs(delta.TotalSeconds), 2)} s longer than expected.");

                    ConsoleEx.WriteLine(
                        $"More accurately a maximum of {max - absolute:N0} can be processed.",
                        ConsoleColor.Cyan);
                    max = max - absolute;
                }
                else
                {
                    ConsoleEx.WriteLine($"It took {Math.Round(delta.TotalSeconds, 2)} s less than expected.");
                    ConsoleEx.WriteLine(
                        $"More accurately a maximum of {max + absolute:N0} can be processed.",
                        ConsoleColor.Cyan);
                    max = max + absolute;
                }
            }

            // Get the best value (closest to one minute => lowest delta)
            int maximum = int.MinValue;
            double min = double.MaxValue;
            foreach (KeyValuePair<TimeSpan, int> value in results)
            {
                if (value.Key.Duration().TotalSeconds > min) continue;
                min = value.Key.Duration().TotalSeconds;
                maximum = value.Value;
            }

            ConsoleEx.WriteBreakline('=', ConsoleColor.Cyan);
            ConsoleEx.WriteLine($"{maximum:N0} can be sorted in exactly one minute.", ConsoleColor.Green);
        }

        private TimeSpan PerformRun(int pass, SortingAlgorithmBase algorithm, int max)
        {
            if (pass <= 0) throw new ArgumentOutOfRangeException(nameof(pass));
            ConsoleEx.WriteBreakline();
            ConsoleEx.WriteLine($"Performing test run {pass}. This will take a minute ...", ConsoleColor.Yellow);
            return TestRun(algorithm, max);
        }

        ///=================================================================================================
        /// <summary>   Determines the base time for 1000 numbers. </summary>
        ///
        /// <param name="algorithm">    The algorithm. </param>
        /// <param name="baseCount">    Number of bases. </param>
        ///
        /// <returns>   A Task&lt;TimeSpan&gt; </returns>
        ///=================================================================================================
        private TimeSpan DetermineBaseTime(SortingAlgorithmBase algorithm, int baseCount)
        {
            Stopwatch watch = new Stopwatch();
            int[] testArray = new int[baseCount];
            testArray.FillRandom();

            watch.Start();
            algorithm.Sort(ref testArray);
            watch.Stop();

            return watch.Elapsed;
        }

        private TimeSpan TestRun(SortingAlgorithmBase algorithm, int amount)
        {
            int[] testArray = new int[amount];
            testArray.FillRandom();

            Stopwatch stopwatch = new Stopwatch();
            startTime = DateTime.Now;
            Timer timer = new Timer(1000);
            timer.Elapsed += TimerOnElapsed;
            timer.Start();

            stopwatch.Start();
            algorithm.Sort(ref testArray);
            stopwatch.Stop();
            timer.Stop();
            timer.Dispose();

            return stopwatch.Elapsed;
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            TimeSpan delta = DateTime.Now - startTime;
            ConsoleEx.RenderProgress((int) delta.TotalSeconds, 60);
        }
    }
}