﻿using System;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task4 : TaskBase
    {
        public override int SheetNumber => 1;
        public override int TaskNumber => 4;
        
        public override string DescriptiveName => "Time and action measurment.";
        
        public override void ExecuteTask()
        {
            int factor = 2;
            int @switch = 0;
            while (true)
            {
                Task4MatrixEx matrix = new Task4MatrixEx(factor, factor);
                matrix.Randomize();

                Task4MatrixEx complementary = new Task4MatrixEx(factor, factor);
                complementary.Randomize();

                matrix.Add(complementary);

                ConsoleEx.WriteLine($"Time for {factor}x{factor}: {matrix.LastActionTime.TotalSeconds} | {matrix.LastActionCount} actions done.", ConsoleColor.Cyan);

                if (matrix.LastActionTime.Minutes >= 1 & @switch == 0)
                {
                    ConsoleEx.WriteLine($"After one minute execution time: {factor}x{factor}.", ConsoleColor.Yellow);
                    @switch++;
                }
                if (matrix.LastActionTime.Minutes >= 2 & @switch == 1)
                {
                    ConsoleEx.WriteLine($"After one minute execution time: {factor}x{factor}.", ConsoleColor.Yellow);
                    @switch++;
                }
                if (matrix.LastActionTime.Minutes >= 5 & @switch == 2)
                {
                    ConsoleEx.WriteLine($"After one minute execution time: {factor}x{factor}.", ConsoleColor.Yellow);
                    @switch++;
                }
                if (matrix.LastActionTime.Minutes >= 10 & @switch == 3)
                {
                    ConsoleEx.WriteLine($"After one minute execution time: {factor}x{factor}.", ConsoleColor.Yellow);
                    break;
                }

                GC.Collect();

                factor *= 2;
            }
        }
    }
}
