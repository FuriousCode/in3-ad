﻿using System;

namespace AD_I3
{
    /// <summary>   The abstract matrix data type for Task 3. </summary>
    public abstract class Task3MatrixClass
    {
        private int _m;
        private int _n;

        /// <summary>   The columns. </summary>
        public int Columns => _n;

        /// <summary>   The rows. </summary>
        public int Rows => _m;

        ///=================================================================================================
        /// <summary>   Specialised constructor for use only by derived class. </summary>
        ///
        /// <param name="m">    The int to process. </param>
        /// <param name="n">    The int to process. </param>
        ///=================================================================================================
        protected Task3MatrixClass(int m, int n)
        {
            if (m < 1) throw new ArgumentOutOfRangeException(nameof(m), "Must specify a value greater than 0.");
            if (n < 1) throw new ArgumentOutOfRangeException(nameof(n), "Must specify a value greater than 0.");

            _m = m;
            _n = n;
        }

        /// <summary>   Initializes this instance with 0-values. </summary>
        public abstract void Init();

        /// <summary>   Prints this matrix. </summary>
        public abstract void Print();

        /// <summary>   Inputs values for this matrix from the Console. </summary>
        public abstract void Input();

        ///=================================================================================================
        /// <summary>   Gets. </summary>
        ///
        /// <param name="m">    The int to process. </param>
        /// <param name="n">    The int to process. </param>
        ///
        /// <returns>   A double. </returns>
        ///=================================================================================================
        public abstract double Get(int m, int n);

        ///=================================================================================================
        /// <summary>   Sets. </summary>
        ///
        /// <param name="m">        The int to process. </param>
        /// <param name="n">        The int to process. </param>
        /// <param name="value">    The value. </param>
        ///=================================================================================================
        public abstract void Set(int m, int n, double value);

        ///=================================================================================================
        /// <summary>   Adds the given matrix to this one. </summary>
        ///
        /// <param name="matrix">   The matrix to add. </param>
        ///
        /// <returns>   The Task3MatrixClass. </returns>
        ///=================================================================================================
        public abstract Task3MatrixClass Add(Task3MatrixClass matrix);

        ///=================================================================================================
        /// <summary>   Multiplies the given matrix with the current one. </summary>
        ///
        /// <param name="matrix">   The matrix to multiply. </param>
        ///
        /// <returns>   The Task3MatrixClass. </returns>
        ///=================================================================================================
        public abstract Task3MatrixClass Mult(Task3MatrixClass matrix);
    }
}
