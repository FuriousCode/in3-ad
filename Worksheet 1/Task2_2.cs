﻿using System;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task2_2 : TaskBase
    {
        public override int SheetNumber => 1;
        public override int TaskNumber => 2;
        public override int? SubtaskNumber => 2;
        public override string DescriptiveName => "Dynamically output all primes to 'k'.";
        
        public override void ExecuteTask()
        {
        start:
            ConsoleEx.WriteLine("Enter a natural number 'k' >= 1:", ConsoleColor.Yellow);
            string input = Console.ReadLine();
            int k;
            if (!int.TryParse(input, out k))
            {
                Console.Clear();
                ConsoleEx.WriteLine("Invalid input. Try again!", ConsoleColor.Red);
                goto start;
            }

            if (k < 1)
            {
                Console.Clear();
                ConsoleEx.WriteLine("'k' was lower than 1. Try again. You can do it!", ConsoleColor.Red);
                goto start;
            }

            int[] primes = Task2Algorithms.EratosthenesAlgorithm(k);
            string aggregate = string.Empty;

            int c = 0;
            foreach (int prime in primes)
            {
                if (c >= 10)
                {
                    // Flush
                    Console.WriteLine(aggregate);
                    aggregate = string.Empty;
                    c = 0;
                }

                if (string.IsNullOrEmpty(aggregate))
                    aggregate += prime.ToString();
                else aggregate += $"{prime,8}";

                c++;
            }

            if (!string.IsNullOrEmpty(aggregate))
                Console.WriteLine(aggregate);
        }
    }
}
