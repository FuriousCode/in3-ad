﻿using System;
using BaseFramework;

namespace AD_I3
{
    ///=================================================================================================
    /// <summary>   Task 1. This class cannot be inherited. </summary>
    ///
    /// <seealso cref="T:BaseFramework.TaskBase"/>
    ///=================================================================================================
    public sealed class Task1 : TaskBase
    {
        /// <summary>   The sheet number. </summary>
        public override int SheetNumber => 1;
        /// <summary>   The task number. </summary>
        public override int TaskNumber => 1;

        public override string DescriptiveName => "Lowest common factor and highest common factor";

        ///=================================================================================================
        /// <summary>   Executes the task. </summary>
        ///
        /// <seealso cref="M:BaseFramework.TaskBase.ExecuteTask()"/>
        ///=================================================================================================
        public override void ExecuteTask()
        {
            for (int a = 30; a <= 40; a++)
            {
                for (int b = 30; b <= 40; b++)
                {
                    int lcf = GetLCF(a, b);
                    int hcf = GetHCF(a, b);
                    int prod = a * b;
                    string output = $"a: {a,4}\tb: {b,4}\tLCF: {lcf,4}\tHCF: {hcf,4}\tPRD: {prod,4}";
                    Console.WriteLine(output);
                }
            }
        }

        ///=================================================================================================
        /// <summary>   Calculates the lowest common factor of two natural numbers. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one of both numbers is lower than
        ///                                                 1. </exception>
        ///
        /// <param name="a">    The natural number 'a'. </param>
        /// <param name="b">    The natural number 'b'. </param>
        ///
        /// <returns>   The lowest common factor of 'a' and 'b'. </returns>
        ///=================================================================================================
        public static int GetLCF(int a, int b)
        {
            if (a <= 0) throw new ArgumentOutOfRangeException(nameof(a), "Only numbers higher than 0 are allowed.");
            if (b <= 0) throw new ArgumentOutOfRangeException(nameof(b), "Only numbers higher than 0 are allowed.");

            int l, lB, h, hB;
            l = lB = Math.Min(a, b);
            h = hB = Math.Max(a, b);

            while (l != h)
            {
                if (l > h) h = h + hB;
                l = l + lB;
            }
            return l;
        }

        ///=================================================================================================
        /// <summary>   Calculates the highest common factor of two natural numbers. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one of both numbers is lower than
        ///                                                 1. </exception>
        ///
        /// <param name="a">    The natural number 'a'. </param>
        /// <param name="b">    The natural number 'b'. </param>
        ///
        /// <returns>   The highest common factor of 'a' and 'b'. </returns>
        ///=================================================================================================
        public static int GetHCF(int a, int b)
        {
            if (a <= 0) throw new ArgumentOutOfRangeException(nameof(a), "Only numbers higher than 0 are allowed.");
            if (b <= 0) throw new ArgumentOutOfRangeException(nameof(b), "Only numbers higher than 0 are allowed.");

            int r;
            do
            {
                r = a % b;
                a = b;
                b = r;
            } while (r > 0);

            return a;
        }

        ///=================================================================================================
        /// <summary>   Calculates the highest common factor of two natural numbers recursively. </summary>
        ///
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one of both numbers is lower than
        ///                                                 1. </exception>
        ///
        /// <param name="a">    The natural number 'a'. </param>
        /// <param name="b">    The natural number 'b'. </param>
        ///
        /// <returns>   The highest common factor of 'a' and 'b'. </returns>
        ///=================================================================================================
        public static int GetHCFRecursive(int a, int b)
        {
            if (a <= 0) throw new ArgumentOutOfRangeException(nameof(a), "Only numbers higher than 0 are allowed.");
            if (b <= 0) throw new ArgumentOutOfRangeException(nameof(b), "Only numbers higher than 0 are allowed.");

            int r = a % b;
            return r == 0 ? a : GetHCFRecursive(b, r);
        }
    }
}
