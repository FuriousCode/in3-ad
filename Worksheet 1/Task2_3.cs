﻿using System;
using System.Diagnostics;
using System.IO;
using BaseFramework;

namespace AD_I3
{
    public class Task2_3 : TaskBase
    {
        public override int SheetNumber => 1;
        public override int TaskNumber => 2;
        public override int? SubtaskNumber => 3;
        public override string DescriptiveName => "Output all primes to 100000.";
        
        public override void ExecuteTask()
        {
            ConsoleEx.WriteLine("Writing down primes to 100000 into 'primes.txt' ...", ConsoleColor.Yellow);
            int[] primes = Task2Algorithms.EratosthenesAlgorithm(100000);

            string path = Path.Combine(Environment.CurrentDirectory, "primes.txt");
            using (StreamWriter sw = new StreamWriter(path))
            {
                int c = 0;
                foreach (int i in primes)
                {
                    sw.Write(i);
                    sw.Write("\t");
                    c++;
                    if (c == 18)
                    {
                        c = 0;
                        sw.Write("\n");
                    }
                }
            }

            Console.WriteLine("Primes written to 'primes.txt'. Press any key to open the file.");
            Console.ReadLine();
            Process.Start(path);
        }
    }
}
