﻿using System;
using System.Diagnostics;
using BaseFramework;

namespace AD_I3
{
    ///=================================================================================================
    /// <summary>   An extended matrix with time and action measurement. </summary>
    ///
    /// <seealso cref="T:AD_I3.Task3MatrixClass"/>
    ///=================================================================================================
    class Task4MatrixEx : Task3MatrixClass
    {
        protected readonly double[] _valueTable;

        ///=================================================================================================
        /// <summary>   Gets or sets the last action time. </summary>
        ///
        /// <value> The last action time. </value>
        ///=================================================================================================
        public TimeSpan LastActionTime { get; private set; }

        ///=================================================================================================
        /// <summary>   Gets or sets the number of last actions. </summary>
        ///
        /// <value> The number of last actions. </value>
        ///=================================================================================================
        public long LastActionCount { get; private set; }

        private readonly Stopwatch _stopwatch;

        ///=================================================================================================
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="m">    The int to process. </param>
        /// <param name="n">    The int to process. </param>
        ///=================================================================================================
        public Task4MatrixEx(int m, int n) : base(m, n)
        {
            _valueTable = new double[m*n];
            _stopwatch = new Stopwatch();
        }

        ///=================================================================================================
        /// <summary>   Initializes this instance with 0-values. </summary>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Init()"/>
        ///=================================================================================================
        public override void Init()
        {
            for (int i = 0; i < _valueTable.Length; i++)
                _valueTable[i] = 0;
        }

        ///=================================================================================================
        /// <summary>   Prints this matrix. </summary>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Print()"/>
        ///=================================================================================================
        public override void Print()
        {
            for (int n = 0; n < Columns; n++)
            {
                for (int m = 0; m < Rows; m++)
                {
                    Console.Write(_valueTable[m*Columns + n]);
                    Console.Write("     ");
                }
                Console.WriteLine();
            }
        }

        ///=================================================================================================
        /// <summary>   Inputs values for this matrix from the Console. </summary>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Input()"/>
        ///=================================================================================================
        public override void Input()
        {
            ConsoleEx.WriteLine("Enter the values like this: \"m1n1 ... mXn1 m1n2 m2n2 ... mXnX\"");
            for (int m = 0; m < Rows; m++)
            {
                for (int n = 0; n < Columns; n++)
                {
                    input:
                    Console.WriteLine($"Enter value for m{m + 1}n{n + 1}:");
                    string input = Console.ReadLine();

                    double value;
                    if (!double.TryParse(input, out value))
                    {
                        ConsoleEx.WriteLine("Invalid input. Try again.", ConsoleColor.Red);
                        goto input;
                    }

                    _valueTable[m*Columns + n] = value;
                }
            }
        }

        ///=================================================================================================
        /// <summary>   Gets. </summary>
        ///
        /// <param name="m">    The int to process. </param>
        /// <param name="n">    The int to process. </param>
        ///
        /// <returns>   A double. </returns>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Get(int,int)"/>
        ///=================================================================================================
        public override double Get(int m, int n)
        {
            LastActionCount++;
            return _valueTable[m*Columns + n];
        }

        ///=================================================================================================
        /// <summary>   Sets. </summary>
        ///
        /// <param name="m">        The int to process. </param>
        /// <param name="n">        The int to process. </param>
        /// <param name="value">    The value. </param>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Set(int,int,double)"/>
        ///=================================================================================================
        public override void Set(int m, int n, double value)
        {
            LastActionCount++;
            _valueTable[m*Columns + n] = value;
        }

        ///=================================================================================================
        /// <summary>   Adds the given matrix to this one. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="matrix">   The matrix to add. </param>
        ///
        /// <returns>   The Task3MatrixClass. </returns>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Add(Task3MatrixClass)"/>
        ///=================================================================================================
        public override Task3MatrixClass Add(Task3MatrixClass matrix)
        {
            if (Columns != matrix.Columns || Rows != matrix.Rows)
                throw new InvalidOperationException("Cannot add two differently sized matrices.");

            _stopwatch.Restart();
            LastActionCount = 0;

            Task3MatrixClass c = new Task3Matrix(Columns, Rows);
            c.Init();
            for (int m = 0; m < Rows; m++)
            {
                for (int n = 0; n < Columns; n++)
                {
                    c.Set(m, n, Get(m, n) + matrix.Get(m, n));
                    LastActionCount += 2;
                }
            }

            _stopwatch.Stop();
            LastActionTime = _stopwatch.Elapsed;

            return c;
        }

        ///=================================================================================================
        /// <summary>   Multiplies the given matrix with the current one. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="matrix">   The matrix to multiply. </param>
        ///
        /// <returns>   The Task3MatrixClass. </returns>
        ///
        /// <seealso cref="M:AD_I3.Task3MatrixClass.Mult(Task3MatrixClass)"/>
        ///=================================================================================================
        public override Task3MatrixClass Mult(Task3MatrixClass matrix)
        {
            if (Columns != matrix.Rows)
                throw new InvalidOperationException("Cannot multiply two differently sized matrices.");

            _stopwatch.Restart();
            LastActionCount = 0;

            Task3MatrixClass c = new Task3Matrix(Rows, matrix.Columns);
            c.Init();

            for (int i = 0; i < c.Rows; i++)
            {
                for (int j = 0; j < c.Columns; j++)
                {
                    for (int k = 0; k < Columns; k++)
                    {
                        c.Set(i, j, c.Get(i, j) + (Get(i, j)*matrix.Get(k, j)));
                        LastActionCount += 3;
                    }
                }
            }

            _stopwatch.Stop();
            LastActionTime = _stopwatch.Elapsed;

            return c;
        }

        /// <summary>   Randomizes this matrix. </summary>
        public void Randomize()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    _valueTable[i*Columns + j] = rnd.NextDouble()*10;
                }
            }
        }
    }
}