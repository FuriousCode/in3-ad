﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task3_2 : TaskBase
    {
        public override int SheetNumber => 1;
        public override int TaskNumber => 3;
        public override int? SubtaskNumber => 2;

        public override string DescriptiveName
            => "An implementation of the abstract matrix class.";

        
        public override void ExecuteTask()
        {
            ConsoleEx.WriteLine("Please enter the matrix row size (m):", ConsoleColor.Yellow);
            int rows = int.Parse(Console.ReadLine());
            ConsoleEx.WriteLine("Please enter the matrix column size (n):", ConsoleColor.Yellow);
            int cols = int.Parse(Console.ReadLine());

            Task3Matrix matrix = new Task3Matrix(rows, cols);
            matrix.Input();

            ConsoleEx.WriteLine(
                "Two random matrices will be generated now, which then get added or multiplied to yours.");
            Task3Matrix addMatrix = new Task3Matrix(rows, cols);
            addMatrix.Randomize();

            ConsoleEx.WriteLine("Add matrix:", ConsoleColor.Cyan);
            addMatrix.Print();

            Task3Matrix mulMatrix = new Task3Matrix(cols, rows);
            mulMatrix.Randomize();

            Console.WriteLine();
            ConsoleEx.WriteLine("Mul matrix:", ConsoleColor.Cyan);
            addMatrix.Print();

            Console.WriteLine();
            ConsoleEx.WriteLine("Result addition:", ConsoleColor.Cyan);
            matrix.Add(addMatrix).Print();

            Console.WriteLine();
            ConsoleEx.WriteLine("Result multiplication:", ConsoleColor.Cyan);
            matrix.Mult(mulMatrix).Print();
        }
    }
}
