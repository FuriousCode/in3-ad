﻿using System;
using BaseFramework;

namespace AD_I3
{
    public sealed class Task3_1 : TaskBase
    {
        public override int SheetNumber => 1;
        public override int TaskNumber => 3;
        public override int? SubtaskNumber => 1;
        public override string DescriptiveName => "An abstract matrix data type definition.";
        public override bool NonExecutable => true;

        public override void ExecuteTask()
        {
            throw new NotSupportedException();
        }
    }
}
