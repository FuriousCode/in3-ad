﻿namespace AD_I3
{
    /// <summary>   Contains all algorithms for Task 2. </summary>
    public static class Task2Algorithms
    {
        ///=================================================================================================
        /// <summary>
        ///     Calculates all prime numbers to 'threshold' by using the Eratosthenes algorithm.
        /// </summary>
        ///
        /// <param name="threshold">    The threshold. </param>
        ///
        /// <returns>   An int[] which contains . </returns>
        ///=================================================================================================
        public static int[] EratosthenesAlgorithm(int threshold)
        {
            // Generate and fill one-dimensional table. 
            int[] map = new int[threshold - 1];

            for (int i = 2; i < map.Length + 2; i++)
            {
                map[i - 2] = i;
            }

            // Iterate table
            int r = 0;
            for (int i = 0; i < map.Length; i++)
            {
                int current = map[i];
                if (current == 0) continue;

                for (int x = i + 1; x < map.Length; x++)
                {
                    if (map[x] % current == 0)
                        map[x] = 0;
                }

                r++;
            }

            // Filter results
            int[] absMap = new int[r];
            int p = 0;
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] == 0) continue;
                absMap[p] = map[i];
                p++;
            }

            return absMap;
        }
    }
}
