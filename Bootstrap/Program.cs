﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using BaseFramework;
using BaseFramework.UI;

namespace AD_I3
{
    class Program
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole(); // Create console window

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow(); // Get console window handle

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        [STAThread]
        static void Main(string[] args)
        {
            CrossThreadApplication app = new CrossThreadApplication();
            ConsoleHandler.Show(true);
            app.Run(new BackgroundHostWindow()
            {
                Width = 0,
                Height = 0,
                WindowStyle = WindowStyle.None,
                ShowInTaskbar = false,
                ShowActivated = false
            });
        }
    }
}
